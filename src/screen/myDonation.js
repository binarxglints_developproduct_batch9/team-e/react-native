import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView, FlatList, TouchableOpacity} from 'react-native';
import TopBarPage from '../component/topBarPage';
import BottomButton from '../component/bottomButton';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import amountParser from '../component/amountParser';
import AsyncStorage from '@react-native-async-storage/async-storage';
function Item({title, message, onPress, amount, isVerified}) {
  return (
    <TouchableOpacity activeOpacity={0.9} onPress={onPress}>
      <View
        style={{
          marginHorizontal: 32,
          flexGrow: 1,
          paddingTop: 16,
          paddingHorizontal: 12,
          elevation: 3,
          backgroundColor: '#fff',
          borderRadius: 7,
          marginBottom: 24,
          alignItems: 'flex-start',
        }}>
        <View style={{flexDirection: 'row'}}>
          <View style={{flex: 3}}>
            <Text
              style={{
                fontWeight: 'bold',
                textDecorationLine: 'underline',
                fontSize: 16,
                color: '#000',
              }}>
              {title}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignSelf: 'flex-end',
              marginLeft: 16,
            }}>
            <Text
              style={{
                color: '#9f9f9f',
                textTransform: 'uppercase',
              }}>
              {!isVerified ? 'Pending' : 'Verified'}
            </Text>
          </View>
        </View>

        <View
          style={{
            marginTop: 8,
          }}>
          <Text
            style={{
              fontSize: 20,
              color: '#1D94A8',
              fontWeight: 'bold',
            }}>
            IDR {amountParser(amount)}
          </Text>
        </View>
        <View>
          <Text
            style={{
              letterSpacing: 0.5,
            }}>
            {message}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

export default function myDonation({navigation}) {
  const dispatch = useDispatch();
  const [Post, setPost] = useState([]);
  const userID = useSelector((state) => state.userID);
  useEffect(() => {
    axios({
      method: 'GET',
      url: `https://talikasih.kuyrek.com:3002/donation/user/?user_id=${userID}&page=1&limit=10
        `,
    })
      .then((e) => {
        setPost(e.data.data);
      })
      .catch((e) => console.log(e));
    // console.log(Post);
  }, []);
  const renderItem = ({item}) => {
    return (
      <Item
        onPress={() => {
          const token = AsyncStorage.getItem('token');
          token.then((e) => {
            dispatch({
              type: 'CAMPAIGN_DATA_REQUIRED',
              campaign_data: {
                id: item._id,
                category: item.category,
                token: e,
              },
            });
          });
          navigation.navigate('DonatePage');
        }}
        title={item.campaign.title}
        message={item.message}
        amount={item.amount}
        isVerified={item.isVerified}
      />
    );
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <TopBarPage nav={navigation} title={'My Donation' + `(${Post.length})`} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            marginTop: 32,
          }}>
          <FlatList
            data={Post}
            keyExtractor={Post.id}
            renderItem={renderItem}
          />
        </View>
      </ScrollView>
      <View>
        <BottomButton
          title="donate"
          onPress={() =>
            navigation.navigate('BottomTabbed', {screen: 'Donate'})
          }
        />
      </View>
    </View>
  );
}
