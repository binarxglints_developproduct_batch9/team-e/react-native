import React, {useEffect, useState} from 'react';
import {View, ScrollView, FlatList} from 'react-native';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Import Screen
import TopBarPage from '../component/topBarPage';
import BottomButton from '../component/bottomButton';
import Item from '../component/myCampaignCardStatus';
import amountParser from '../component/amountParser';

export default function myCampaign({navigation}) {
  // GET data for campaign card
  const [Post, setPost] = useState([]);
  const dispatch = useDispatch();
  const userID = AsyncStorage.getItem('userID');
  useEffect(() => {
    userID.then((ID) => {
      axios({
        method: 'GET',
        url: `https://talikasih.kuyrek.com:3001/campaign/user?user_id=${ID}&page=1&limit=10`,
      })
        .then((res) => {
          setPost(res.data.data);
        })
        .catch((e) => console.log(e));
    });
  }, []);
  const renderItem = ({item}) => {
    return (
      <Item
        image={item.images}
        title={item.title}
        category={item.category}
        progress={item.total_donation_rupiah / item.goal}
        user={item.user.name}
        raised={amountParser(item.total_donation_rupiah)}
        goal={amountParser(item.goal)}
        wallet={amountParser(item.wallet)}
        status={item.status}
        onPress={() => {
          dispatch({type: 'fundraise', fundraise: true});
          const token = AsyncStorage.getItem('token');
          token.then((e) => {
            dispatch({
              type: 'CAMPAIGN_DATA_REQUIRED',
              campaign_data: {
                id: item._id,
                category: item.category,
                token: e,
              },
            });
          });
          navigation.navigate('DonatePage');
        }}
      />
    );
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <View>
        <TopBarPage
          title={'My Campaign' + `(${Post.length})`}
          nav={navigation}
        />
      </View>
      <ScrollView>
        <FlatList
          data={Post}
          keyExtractor={Post.id}
          renderItem={renderItem}
          style={{
            marginVertical: 24,
          }}
        />
      </ScrollView>
      <View>
        <BottomButton
          title="Create new campaign"
          onPress={() => {
            navigation.navigate('BottomTabbed', {screen: 'Create Campaign'});
            dispatch({type: 'fundraiser', fundraise: true});
          }}
        />
      </View>
    </View>
  );
}
