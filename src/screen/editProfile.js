import React, {useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Image,
  ToastAndroid,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Collapsible from 'react-native-collapsible';
import {useDispatch, useSelector} from 'react-redux';

import InputProfile from '../component/inputProfile';
import TopBarPage from '../component/topBarPage';
import BottomButton from '../component/bottomButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {launchImageLibrary} from 'react-native-image-picker';
import LottieView from 'lottie-react-native';

const screenWidth = Dimensions.get('window').width;

function backNumberParser() {
  let val = '043301004131501' + '0';
  let get = val.slice(-4, -1);
  return '*******' + get;
}

export default function editProfile({navigation}) {
  const [collapsed, setCollapsed] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [Name, setName] = useState('');
  const [Email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repPass, setRepPass] = useState('');
  const [bankName, setBankName] = useState('');
  const [bankNumber, setBankNumber] = useState('');
  const userData = useSelector((state) => state.userData);
  const [response, setResponse] = useState(
    require('../../Asset/icons8-person-64.png'),
  );
  const token = AsyncStorage.getItem('token');
  const dispatch = useDispatch();
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <TopBarPage nav={navigation} title="Edit Profile" />
      <ScrollView>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 24,
            marginHorizontal: 16,
          }}>
          <View>
            <Image
              source={response}
              style={{
                width: screenWidth - 160,
                height: 200,
                resizeMode: 'contain',
                borderRadius: 10,
              }}
            />
          </View>
          <View
            style={{
              marginTop: 8,
            }}>
            <TouchableOpacity
              onPress={() =>
                launchImageLibrary(
                  {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxWidth: screenWidth - 160,
                    maxHeight: 200,
                  },
                  (resp) => setResponse(resp),
                )
              }>
              <Text
                style={{
                  color: '#1D94A8',
                  textDecorationLine: 'underline',
                  fontWeight: 'bold',
                  fontSize: 16,
                }}>
                Change Profile Image
              </Text>
            </TouchableOpacity>
          </View>
          <View>
            <InputProfile
              placeholder={userData.name}
              Title="Name"
              editable={true}
              span="*"
              colors={false}
              fontWeight="bold"
              onChangeText={(Name) => setName(Name)}
            />
          </View>
          <View>
            <InputProfile
              placeholder={userData.email}
              Title="Email"
              span="*"
              colors={false}
              editable={true}
              fontWeight="bold"
              onChangeText={(email) => setEmail(email)}
            />
          </View>
          <View
            style={{
              alignSelf: 'flex-end',
              marginTop: 8,
            }}>
            <TouchableOpacity
              onPress={() => setCollapsed(collapsed ? false : true)}
              activeOpacity={0.9}>
              <Text
                style={{
                  fontWeight: 'bold',
                  textDecorationLine: 'underline',
                }}>
                Reset Password
              </Text>
            </TouchableOpacity>
          </View>
          <Collapsible collapsed={collapsed}>
            <View>
              <View>
                <InputProfile
                  placeholder="Password"
                  Title="Password"
                  span="*"
                  onChangeText={(pass) => setPassword(pass)}
                  colors={false}
                  editable={true}
                  fontWeight="bold"
                  secure={true}
                />
              </View>
              <View>
                <InputProfile
                  placeholder="Repeat Password"
                  Title="Repeat Password"
                  span="*"
                  colors={false}
                  editable={true}
                  onChangeText={(RepPass) => setRepPass(RepPass)}
                  fontWeight="bold"
                  secure={true}
                />
              </View>
            </View>
          </Collapsible>
          <View
            style={{
              marginTop: 36,
              marginBottom: 48,
            }}>
            <View>
              <Text
                style={{
                  color: '#A87B14',
                  fontSize: 14,
                }}>
                We need your Bank Account for campaign purpose
              </Text>
            </View>
            <View>
              <InputProfile
                placeholder="BCA"
                Title="Bank Name"
                editable={true}
                onChangeText={(bank_name) => setBankName(bank_name)}
                span="*"
                colors={false}
                editable={true}
                fontWeight="bold"
              />
            </View>
            <View>
              <InputProfile
                placeholder={backNumberParser()}
                Title="Bank Account Number"
                onChangeText={(bank_account_number) =>
                  setBankNumber(bank_account_number)
                }
                editable={true}
                span="*"
                colors={false}
                editable={true}
                fontWeight="bold"
              />
            </View>
          </View>
        </View>
        <Modal transparent={true} visible={isVisible}>
          <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <View
              style={{
                paddingHorizontal: 16,
                width: screenWidth - 64,
                padding: 16,
                backgroundColor: '#fff',
                elevation: 4,
                borderRadius: 10,
                alignItems: 'center',
              }}>
              <View
                style={{
                  position: 'absolute',
                  alignSelf: 'flex-end',
                  marginRight: 16,
                  paddingHorizontal: 16,
                  marginVertical: 16,
                }}>
                <TouchableOpacity onPress={() => setIsVisible(false)}>
                  <Icon name="close" size={24} />
                </TouchableOpacity>
              </View>
              <View style={{width: 200, height: 200}}>
                <LottieView
                  source={require('../../Asset/JSON Animation/profile_update.json')}
                  autoPlay
                  loop
                  style={{
                    width: 200,
                    height: 200,
                  }}
                />
              </View>
              <Text
                style={{
                  fontSize: 16,
                  textTransform: 'uppercase',
                  letterSpacing: 1,
                }}>
                {' '}
                Profile Updated!{' '}
              </Text>
            </View>
          </View>
        </Modal>
      </ScrollView>

      <BottomButton
        title="save changes"
        onPress={() => {
          token.then((e) => {
            // Email Validation
            Email !== '' &&
              dispatch({
                type: 'UPDATE_EMAIL_REQUESTED',
                payload: {email: Email, token: e, visible: setIsVisible},
              });
            // BankInfo validation
            Name !== '' &&
              dispatch({
                type: 'UPDATE_BANKINFO_REQUESTED',
                payload: {
                  name: Name,
                  bank_name: bankName,
                  bank_number: bankNumber,
                  token: e,
                  visible: setIsVisible,
                },
              });
            // UPDATE PROFILE IMAGE
            !response === '../../Asset/icons8-person-64.png' &&
              dispatch({
                type: 'UPDATE_PROFILE_IMAGE_REQUESTED',
                payload: {image: response, token: e, visible: setIsVisible},
              });
            // Update Password
            (password === repPass && password, repPass !== '') &&
              dispatch({
                type: 'UPDATE_PASSWORD_REQUESTED',
                payload: {password, repPass, token: e, visible: setIsVisible},
              });
          });
        }}
      />
    </View>
  );
}
