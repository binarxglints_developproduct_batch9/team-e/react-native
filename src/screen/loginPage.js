import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/Feather';
import {useDispatch} from 'react-redux';
import LottieView from 'lottie-react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  GoogleSignin,
  GoogleSigninButton,
} from '@react-native-community/google-signin';
import {installed} from '../../Asset/google-signIn/googleSignIn_configuration.json';
const ScreenWidth = Dimensions.get('window').width;

export default function loginPage({navigation}) {
  const dispatch = useDispatch();
  const [Loading, setLoading] = useState(false);
  const [Email, setEmail] = useState('');
  const [Password, setPassword] = useState('');
  const [Visibility, setVisibility] = useState(false);
  const [secure, setSecure] = useState(true);
  const [disabled, setDisabled] = useState(true);
  const [disabledPassword, setDisabledPassword] = useState(true);
  const [isValidUser, setIsValidUser] = useState(true);
  const [isValidPassword, setIsValidPassword] = useState(true);
  const [googleUserInfo, setGoogleUserInfo] = useState([]);
  // TextInput Refs
  let emailInput = null;
  let passwordInput = null;
  // Google Login Function
  async function handleGoogleLogin() {
    await GoogleSignin.hasPlayServices();
    try {
      console.log('works');
      const userData = await GoogleSignin.signIn();
      // setGoogleUserInfo(userData);
      // console.log(userData.user);
    } catch (error) {
      console.log('google login', error);
    }
  }
  // Back Handler Function
  const backButton = () => {
    navigation.navigate('BottomTabbed', {screen: 'Donate'});
  };
  BackHandler.addEventListener('hardwareBackPress', backButton);
  useEffect(() => {
    GoogleSignin.configure({
      // scopes: ['https://www.googleapis.com/auth/userinfo.profile'],
      webClientId:
        '106049491282-8199hindiign90jqo75a9s0dq3nq6893.apps.googleusercontent.com',
      // client_type: 3,
    });
  }, []);
  useEffect(() => {
    BackHandler.removeEventListener('hardwareBackPress', backButton);
  }, [backButton]);
  const passwordValidation = (password) => {
    if (password.length >= 8) {
      setDisabledPassword(false);
      setIsValidPassword(true);
    } else {
      setDisabledPassword(true);
      setIsValidPassword(false);
    }
  };
  const isEmpty = (input) => {
    if (input === '') {
      setIsValidUser(false);
      setDisabled(true);
    } else if (input.length < 14) {
      setIsValidUser(false);
      setDisabled(true);
    } else if (input.length >= 14) {
      setIsValidUser(true);
      setDisabled(false);
    }
  };
  return (
    <ScrollView
      style={{
        flex: 1,
        backgroundColor: '#F1EDE4',
      }}>
      <View style={{flex: 1, backgroundColor: '#F1EDE4', alignItems: 'center'}}>
        <View
          style={{
            padding: 16,
            alignSelf: 'flex-start',
          }}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.goBack()}>
            <Icon name="keyboard-backspace" size={30} color="#1D94A8" />
          </TouchableOpacity>
        </View>
        <Image
          style={{
            aspectRatio: 1,
            height: ScreenWidth - 180,
            width: ScreenWidth - 172,
            marginTop: 10,
            marginBottom: 30,
          }}
          source={require('../../Asset/Logo2.png')}
        />
        <View style={styles.textinput}>
          <TextInput
            placeholder="Email"
            value={Email}
            ref={(input) => {
              emailInput = input;
            }}
            onChangeText={(email) => {
              setEmail(email);
              // console.log(email);
            }}
            onSubmitEditing={() => passwordInput.focus()}
            onEndEditing={(e) => isEmpty(e.nativeEvent.text)}
            keyboardType="email-address"
          />
        </View>
        {!isValidUser && (
          <Text
            style={{
              color: '#A43F3C',
              alignSelf: 'flex-start',
              paddingHorizontal: 32,
            }}>
            This form is required
          </Text>
        )}
        <View style={styles.textinput}>
          <TextInput
            ref={(input) => {
              passwordInput = input;
            }}
            placeholder="Password"
            secureTextEntry={secure}
            onChangeText={(password) => {
              setPassword(password);
              // console.log(password);
            }}
            onEndEditing={(e) => passwordValidation(e.nativeEvent.text)}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                setVisibility(Visibility ? false : true);
                setSecure(secure ? false : true);
              }}>
              <Icons
                name={!Visibility ? 'eye' : 'eye-off'}
                size={24}
                color="#9f9f9f"
              />
            </TouchableOpacity>
          </View>
        </View>
        {!isValidPassword && (
          <View
            style={{
              alignSelf: 'flex-start',
            }}>
            <Text
              style={{
                paddingHorizontal: 32,
                color: '#A43F3C',
              }}>
              Password must be 8 character or more
            </Text>
          </View>
        )}
        <View
          style={{
            alignItems: 'flex-end',
            alignSelf: 'flex-end',
            marginHorizontal: 32,
            marginTop: 8,
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('forgot_password');
            }}>
            <Text style={{fontSize: 14}}>Forgot Password?</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
          }}>
          {Loading ? (
            <View
              style={{
                width: ScreenWidth,
                height: 72,
              }}>
              <LottieView
                source={require('../../Asset/JSON Animation/lf30_editor_bozdlxrs.json')}
                autoPlay
                loop
                style={{
                  flex: 1,
                }}
              />
            </View>
          ) : (
            <TouchableOpacity
              disabled={(disabled, disabledPassword ? true : false)}
              onPress={() => {
                setLoading(true);
                axios({
                  method: 'POST',
                  url: 'https://talikasih.kuyrek.com:3000/user/login',
                  data: {
                    email: Email,
                    password: Password,
                  },
                })
                  .then(async (e) => {
                    await AsyncStorage.setItem('token', e.data.token);
                    await axios({
                      method: 'GET',
                      url: `https://talikasih.kuyrek.com:3000/user/authorization`,
                      headers: {
                        Authorization: `Bearer ${e.data.token}`,
                      },
                    })
                      .then(async (e) => {
                        e.data !== undefined &&
                          (await AsyncStorage.setItem(
                            'userID',
                            e.data.user.id,
                          ));
                        dispatch({
                          type: 'userID',
                          payloadUserID: e.data.user.id,
                        });
                        dispatch({
                          type: 'LOGIN_DATA',
                          loginData: e.data.user,
                        });
                        dispatch({
                          type: 'status',
                          status: true,
                        });
                        setTimeout(() => {
                          setLoading(false);
                          navigation.navigate('BottomTabbed', {
                            screen: 'Donate',
                          });
                        }, 3000);
                      })
                      .catch((e) => {
                        setTimeout(() => {
                          setLoading(false);

                          alert(e);
                        }, 3000);
                      });
                  })
                  .catch((e) => {
                    setTimeout(() => {
                      setLoading(false);
                      alert(e);
                    }, 3000);
                  });
              }}
              style={styles.button}>
              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  fontSize: 16,
                }}>
                LOGIN
              </Text>
            </TouchableOpacity>
          )}
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <Text>New user? </Text>
          <TouchableOpacity>
            <Text
              style={{
                color: '#1D94A8',
                borderBottomWidth: 1,
                borderBottomColor: '#1D94A8',
              }}
              onPress={() => {
                navigation.navigate('Register');
              }}>
              Create an account
            </Text>
          </TouchableOpacity>
        </View>
        {/* <GoogleSigninButton
          onPress={handleGoogleLogin}
          size={GoogleSigninButton.Size.Icon}
          color={GoogleSigninButton.Color.Dark}
        /> */}
      </View>
    </ScrollView>
  );
}

var styles = StyleSheet.create({
  textinput: {
    flexDirection: 'row',
    width: ScreenWidth - 64,
    alignItems: 'center',
    backgroundColor: '#FCFCFC',
    borderBottomWidth: 1,
    marginTop: 20,
    paddingHorizontal: 10,
    paddingVertical: 2,
  },
  button: {
    marginHorizontal: 55,
    alignItems: 'center',
    marginTop: 20,
    width: ScreenWidth - 64,
    backgroundColor: '#A43F3C',
    paddingVertical: 12,
    borderRadius: 5,
  },
});
