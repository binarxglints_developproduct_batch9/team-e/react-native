import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  TextInput,
  Modal,
} from 'react-native';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import Icon from 'react-native-vector-icons/Feather';
import {Picker} from '@react-native-picker/picker';
import {launchImageLibrary} from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import LottieView from 'lottie-react-native';

// Import Component
import TopBar from '../component/topBar';
import Input from '../component/donorInput';
import LoggedOut from '../component/loggedOutAnimated';
import section from '../component/section';

let yearRef = null;
let monthRef = null;
let dayReff = null;

const screenWidth = Dimensions.get('window').width;
const NoResponse = () => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Icon name="plus-circle" color="#9f9f9f" size={32} />
      <Text
        style={{
          color: '#9f9f9f',
          fontSize: 16,
          marginTop: 8,
        }}>
        Add Header Photo
      </Text>
    </View>
  );
};

export default function createCampaign({navigation}) {
  const dispatch = useDispatch();
  const loginStatus = useSelector((state) => state.loginStatus);
  const [isLoading, setIsLoading] = useState(false);
  const [isSucceed, setIsSucceed] = useState(false);
  const [goal, setGoal] = useState(0);
  const [isValidMonth, setIsValidMonth] = useState(false);
  const [isDayValid, setIsDayValid] = useState(false);
  const [year, setYear] = useState(2021);
  const [months, setMonths] = useState(null);
  const [isValidYear, setIsValidYear] = useState(false);
  const [days, setDays] = useState(null);
  const [date, setDate] = useState('');
  let tanggall = new Date(date);
  const [category, setCategory] = useState(null);
  const [response, setResponse] = useState('');
  const [tokenContainer, setTokenContainer] = useState('');
  const [title, setTitle] = useState('');
  const [story, setStory] = useState('');
  const dueDate = `${year}-${months}-${days}`;
  const yearValidation = (year) => {
    if (year >= 2021 && year < 2025) {
      setIsValidYear(true);
    } else {
      setIsValidYear(false);
    }
  };
  const monthValidation = (month) => {
    if (month <= 12 && month >= 1) {
      setIsValidMonth(true);
    } else {
      setIsValidMonth(false);
    }
  };
  const dayValidation = (day) => {
    if (day >= 1 && day <= 31) {
      setIsDayValid(true);
    } else {
      setIsDayValid(false);
    }
  };
  const token = AsyncStorage.getItem('token');
  useEffect(() => {
    token
      .then((e) => setTokenContainer(e))
      .catch((e) => console.log('error :', e));
  }, [loginStatus]);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <TopBar nav={navigation} />
      {/* Component Goes Here */}

      {!loginStatus ? (
        <LoggedOut title="Login to Create Campaign" />
      ) : (
        <ScrollView
          style={{
            flex: 1,
          }}>
          <View
            style={{
              alignItems: 'center',
              marginTop: 24,
              flex: 1,
              paddingHorizontal: 16,
              paddingBottom: 32,
            }}>
            <TouchableOpacity
              onPress={() =>
                launchImageLibrary(
                  {
                    mediaType: 'photo',
                    includeBase64: true,
                    maxHeight: 400,
                    maxWidth: 400,
                  },
                  (resp) => {
                    setResponse(resp);
                  },
                )
              }>
              <View
                style={{
                  backgroundColor: '#E1E0E0',
                  marginBottom: 32,
                  width: screenWidth - 42,
                  height: 200,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {response == '' ? <NoResponse /> : console.log('')}
                <Image
                  source={response}
                  style={{
                    width: response !== '' ? screenWidth - 42 : 0,
                    height: response !== '' ? 200 : 0,
                  }}
                />
              </View>
            </TouchableOpacity>

            <View
              style={{
                marginBottom: 16,
              }}>
              <Input
                onChangeText={(text) => setTitle(text)}
                title="Title"
                placeholder="e.h Help us to get clean water"
              />
            </View>

            <View
              style={{
                marginBottom: 16,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Text style={{fontWeight: 'bold'}}>Category</Text>
                <Text style={{color: '#A43F3C'}}>*</Text>
              </View>

              <View
                style={{
                  borderBottomColor: '#9f9f9f',
                  borderBottomWidth: 2,
                }}>
                <Picker
                  mode="dropdown"
                  selectedValue={category}
                  onValueChange={(value) => setCategory(value)}
                  style={{
                    width: screenWidth - 32,
                    backgroundColor: '#fcfcfc',
                    height: 48,
                  }}>
                  <Picker.Item label="Disability" value="Disability" />
                  <Picker.Item label="Medical" value="Medical" />
                  <Picker.Item label="Religion" value="Religion" />
                  <Picker.Item label="Education" value="Education" />
                  <Picker.Item label="Humanity" value="Humanity" />
                  <Picker.Item label="Environment" value="Environment" />
                  <Picker.Item label="Disaster" value="Disaster" />
                  <Picker.Item label="SocioPreuner" value="SocioPreuner" />
                </Picker>
              </View>
            </View>

            <View
              style={{
                marginBottom: 16,
              }}>
              <Input
                onChangeText={(e) => setGoal(e)}
                title="Goal"
                keyboard="number-pad"
                placeholder="e.h Help us to get clean water"
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'flex-start',
                marginBottom: 16,
              }}>
              <Text style={{fontWeight: 'bold'}}>Due Date</Text>
              <Text style={{color: '#A43F3C'}}>*</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 6}}>
              <TextInput
                ref={(input) => {
                  input = yearRef;
                }}
                placeholder="YYYY"
                maxLength={4}
                keyboardType="number-pad"
                onChangeText={(e) => setYear(e)}
                onEndEditing={(e) => {
                  yearValidation(e.nativeEvent.text);
                }}
                onSubmitEditing={() => monthRef.focus()}
                style={{
                  height: 48,
                  flex: 2,
                  paddingHorizontal: 16,
                  backgroundColor: '#fcfcfc',
                  borderBottomWidth: 2,
                  borderBottomColor: isValidYear ? '#9f9f9f' : '#A43F3C',
                  alignItems: 'center',
                }}
              />

              <TextInput
                placeholder="MM"
                ref={(input) => {
                  monthRef = input;
                }}
                maxLength={2}
                keyboardType="number-pad"
                onChangeText={(e) => {
                  setMonths(e);
                }}
                onEndEditing={(e) => monthValidation(e.nativeEvent.text)}
                onSubmitEditing={() => dayReff.focus()}
                style={{
                  height: 48,
                  flex: 2,
                  paddingHorizontal: 16,
                  backgroundColor: '#fcfcfc',
                  borderBottomWidth: 2,
                  borderBottomColor: isValidMonth ? '#9f9f9f' : '#A43F3C',
                  marginHorizontal: 24,
                }}
              />

              <TextInput
                ref={(input) => {
                  dayReff = input;
                }}
                placeholder="DD"
                maxLength={2}
                keyboardType="number-pad"
                onChangeText={(e) => setDays(e)}
                onEndEditing={(e) => dayValidation(e.nativeEvent.text)}
                style={{
                  flex: 2,
                  height: 48,
                  paddingHorizontal: 16,
                  backgroundColor: '#fcfcfc',
                  borderBottomWidth: 2,
                  borderBottomColor: isDayValid ? '#9f9f9f' : '#A43F3C',
                }}
              />
            </View>

            <View
              style={{
                alignSelf: 'flex-start',
                marginTop: 16,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Text style={{fontWeight: 'bold'}}>Story</Text>
                <Text style={{color: '#A43F3C'}}>*</Text>
              </View>

              <View
                style={{
                  height: 328,
                  marginTop: 16,
                }}>
                <AutoGrowingTextInput
                  onChangeText={(story) => {
                    setStory(story);
                  }}
                  placeholder="Tell your story..."
                  style={{
                    width: screenWidth - 32,
                    backgroundColor: '#fcfcfc',
                    padding: 16,
                  }}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {
                setIsLoading(true);
                dispatch({
                  type: 'CREATE_CAMPAIGN_REQUSTED',
                  payload: {
                    date: dueDate,
                    category,
                    image: response,
                    goal,
                    title,
                    story,
                    token: tokenContainer,
                    setIsValid: setIsSucceed,
                    loading: setIsLoading,
                  },
                });
                setResponse(null);
                setGoal(0);
                setTitle('');
                setStory('');
              }}
              style={{
                width: screenWidth - 32,
                borderRadius: 6,
                marginTop: 24,
                alignItems: 'center',
                paddingVertical: 16,
                backgroundColor: '#A43F3C',
              }}>
              <Text
                style={{
                  fontSize: 16,
                  color: '#fff',
                  fontWeight: 'bold',
                  textTransform: 'uppercase',
                }}>
                Create Campaign
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      )}
      <Modal transparent={true} visible={isLoading}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={{
              width: screenWidth - 144,
              paddingVertical: 16,
              alignItems: 'center',
              backgroundColor: '#fff',
              borderRadius: 6,
            }}>
            <View
              style={{
                width: screenWidth - 200,
                height: 100,
              }}>
              <LottieView
                source={require('../../Asset/JSON Animation/loading.json')}
                autoPlay
                loop
              />
            </View>
          </View>
        </View>
      </Modal>
      <Modal transparent={true} visible={isSucceed}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0.6)',
          }}>
          <View
            style={{
              width: screenWidth - 64,
              padding: 16,
              backgroundColor: '#fff',
              borderRadius: 8,
            }}>
            <View
              style={{
                alignItems: 'center',
              }}>
              <LottieView
                source={require('../../Asset/JSON Animation/create_campaign_response.json')}
                autoplay={true}
                loop={true}
                style={{
                  width: 100,
                  height: 150,
                }}
              />
            </View>
            <Text
              style={{
                fontWeight: 'bold',
                alignSelf: 'center',
              }}>
              Hooray! Campaign Created!
            </Text>
            <View
              style={{
                borderBottomColor: '#9f9f9f',
                marginTop: 8,
                alignSelf: 'center',
                width: '90%',
                height: 1,
                borderBottomWidth: 1,
              }}
            />
            <TouchableOpacity
              onPress={() => {
                setIsSucceed(false);
                navigation.navigate('MyCampaign');
              }}
              style={{
                alignSelf: 'center',
                marginTop: 8,
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 16,
                  letterSpacing: 0.9,
                  color: '#1D94A8',
                }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
}
