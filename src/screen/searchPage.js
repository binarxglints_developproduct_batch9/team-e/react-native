import React from 'react';
import {
  View,
  Text,
  ScrollView,
  FlatList,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import TopBarPage from '../component/topBarPage';
import MiniCard from '../component/miniCard';
import {TextInput} from 'react-native-gesture-handler';
import {useState, useEffect} from 'react';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
const ScreenWidth = Dimensions.get('window').width;

const data = [
  {
    name: 'Disability',
    path: require('../../Asset/Disability.png'),
  },
  {
    name: 'Medical',
    path: require('../../Asset/Medical.png'),
  },
  {
    name: 'Education',
    path: require('../../Asset/Education.png'),
  },
  {
    name: 'Religious',
    path: require('../../Asset/Religious.png'),
  },
  {
    name: 'Humanity',
    path: require('../../Asset/Humanity.png'),
  },
  {
    name: 'Enviroment',
    path: require('../../Asset/Enviroment.png'),
  },
  {
    name: 'Disaster',
    path: require('../../Asset/Disastah.png'),
  },
  {
    name: 'Socioprenour',
    path: require('../../Asset/Socioprenour.png'),
  },
];

const Item = ({title, path}) => {
  return (
    <View
      style={{
        marginHorizontal: 4,
        justifyContent: 'center',
        marginTop: 15,
        alignItems: 'center',
        flex: 2,
        borderWidth: 1,
        elevation: 4,
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        borderColor: '#E1E0E0',
        height: 90,
      }}>
      <Image
        style={{
          resizeMode: 'contain',
          width: 30,
          height: 36,
        }}
        source={path}
      />
      <Text
        style={{
          fontSize: 10,
          letterSpacing: 0.5,
          color: '#9F9F9F',
          marginTop: 10,
        }}>
        {title}
      </Text>
    </View>
  );
};
export default function searchPage({navigation}) {
  function renderItem({item}) {
    return <Item title={item.name} path={item.path} />;
  }

  function renderItem2({item}) {
    const progress =
      item.campaign.total_donation_rupiah === undefined
        ? 0
        : item.campaign.total_donation_rupiah / item.campaign.goal;
    return (
      <MiniCard
        image={item.campaign.images}
        title={item.campaign.title}
        category={item.campaign.category}
        progress={progress}
        raised={item.campaign.total_donation_rupiah}
        goal={item.campaign.goal}
        onPress={() => {
          dispatch({
            type: 'CAMPAIGN_DATA_REQUIRED',
            campaign_data: {
              id: item.campaign._id,
              category: item.campaign.category,
            },
          });

          navigation.navigate('DonatePage');
        }}
      />
    );
  }
  const dispatch = useDispatch();
  const [searchQuery, setSearchQuery] = useState('');
  const userID = useSelector((state) => state.userID);
  const [getData, setGetData] = useState([]);
  const UID = AsyncStorage.getItem('userID');
  useEffect(() => {
    UID.then((e) => {
      axios({
        method: 'GET',
        url: `https://talikasih.kuyrek.com:3001/history/get/user?user_id=${e}&page=1&limit=10`,
      }).then((res) => {
        // console.log(res.data.data);
        setGetData(res.data.data);
      });
    }, []);
    //console.log(getData);
  });

  return (
    <View style={{flex: 1, backgroundColor: '#FAF8F3'}}>
      <TopBarPage title="Explore" nav={navigation} />
      <ScrollView>
        <View style={{paddingHorizontal: 15, marginTop: 32}}>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <View style={{flex: 6, backgroundColor: '#FCFCFC'}}>
              <TextInput
                onChangeText={(query) => setSearchQuery(query)}
                style={{
                  fontSize: 15,
                  borderBottomWidth: 1,
                  paddingHorizontal: 8,
                }}
                placeholder="Search"></TextInput>
            </View>
            <View
              style={{
                paddingHorizontal: 8,
                alignItems: 'center',
                marginLeft: 20,
                flex: 1,
                justifyContent: 'center',
                borderWidth: 1,
                borderRadius: 3,
                backgroundColor: '#FFFFFF',
                borderColor: '#1D94A8',
              }}>
              <TouchableOpacity
                onPress={() => {
                  dispatch({
                    type: 'SEARCH_REQUESTED',
                    payload: {
                      searchQuery,
                      navigation,
                    },
                  });
                }}>
                <Icon name="search" size={32} color="#1D94A8" />
              </TouchableOpacity>
            </View>
          </View>
          {/* <Text style={{color: '#9F9F9F', marginTop: 30, fontSize: 14}}>
            Filter by category
          </Text> */}
        </View>
        {/* <View
          style={{
            flex: 1,
          }}>
          <FlatList
            data={data}
            renderItem={renderItem}
            numColumns={4}
            keyExtractor={data.name}
            style={{flex: 1, paddingHorizontal: 16}}
          />
        </View> */}
        <View style={{paddingHorizontal: 16}}>
          <Text
            style={{
              fontWeight: 'bold',
              textDecorationLine: 'underline',
              fontSize: 16,
              marginTop: 24,
            }}>
            Recently Viewed
          </Text>
        </View>
        <View style={{paddingLeft: 16, paddingRight: 16, marginTop: 16}}>
          <FlatList
            data={getData}
            keyExtractor={getData._id}
            renderItem={renderItem2}
            showsHorizontalScrollIndicator={false}
            horizontal
          />
        </View>
      </ScrollView>
    </View>
  );
}
