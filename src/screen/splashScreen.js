import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useEffect} from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
import {useDispatch} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';

export default function splashScreen({navigation}) {
  const token = AsyncStorage.getItem('token');
  const dispatch = useDispatch();
  useEffect(() => {
    // dispatch({type:'GET_DATA_HOME_PAGE'})
    token.then((e) => {
      e === null
        ? dispatch({type: 'status', status: false})
        : dispatch({type: 'status', status: true});
      setTimeout(() => {
        navigation.navigate('BottomTabbed', {screen: 'Donate'});
      }, 3000);
    });
  }, []);
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={['#F1EDE4', '#E3ECEA', '#D7EBEE']}
        style={styles.linearGradient}>
        <Image
          style={{
            aspectRatio: 1,
            height: ScreenWidth - 180,
            width: ScreenWidth - 172,
            marginTop: 10,
            marginBottom: 30,
          }}
          source={require('../../Asset/Logo2.png')}
        />
      </LinearGradient>
    </View>
  );
}

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: ScreenHeight,
    width: ScreenWidth,
  },
});
