import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  FlatList,
  Image,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Carousel from 'react-native-snap-carousel';
import axios from 'axios';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

//Import Component
import MiniCard from '../component/miniCard';
import TopBar from '../component/topBar';
import CampaignFiltered from '../component/myCampaignCard';
// API
import {API_NO_PARAM_CONFIG} from '../store/reducer/api';
import {data} from '../component/carousselPath';
import amountParser from '../component/amountParser';

const ScreenWidth = Dimensions.get('window').width;
const ScreenHeight = Dimensions.get('window').height;

function renderItemCarousel({item}) {
  return (
    <View style={{marginVertical: 20}}>
      <Image
        source={item.path}
        style={{
          width: ScreenWidth - 32,
          height: 200,
          resizeMode: 'contain',
          borderRadius: 4,
        }}
      />
    </View>
  );
}

function Placeholder() {
  return (
    <SkeletonPlaceholder>
      <View
        style={{
          marginHorizontal: 16,
          marginTop: 24,
        }}>
        <View
          style={{
            marginBottom: 32,
            width: ScreenWidth - 32,
            height: 200,
            borderRadius: 4,
          }}
        />
        <View
          style={{
            alignSelf: 'flex-start',
            marginTop: 16,
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: ScreenWidth - 136,
              height: 330,
              borderRadius: 10,
              marginRight: 16,
            }}
          />
          <View
            style={{
              width: ScreenWidth - 136,
              height: 330,
              borderRadius: 10,
              marginRight: 16,
            }}
          />
        </View>
      </View>
    </SkeletonPlaceholder>
  );
}

export default function homePage({navigation}) {
  // UseState
  const [urgent, getUrgent] = useState([]);
  const [newest, setNewest] = useState([]);
  const [popular, setPopular] = useState([]);
  const [searchResult, setSearchResult] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  // UseSelector
  const reducer = useSelector((state) => state);
  const filter = useSelector((state) => state.filter);
  const urgent_data_response = useSelector(
    (state) => state.urgent_data_response,
  );
  const newest_data_response = useSelector(
    (state) => state.newest_data_response,
  );
  const popular_data_response = useSelector(
    (state) => state.popular_data_response,
  );
  const token = AsyncStorage.getItem('token');
  function renderItem({item}) {
    const progress = item.total_donation_rupiah / item.goal;

    return (
      <MiniCard
        image={item.images}
        title={item.title}
        category={item.category}
        progress={progress}
        raised={amountParser(item.total_donation_rupiah)}
        goal={amountParser(item.goal)}
        onPress={() => {
          token
            .then((e) => {
              dispatch({
                type: 'CAMPAIGN_DATA_REQUIRED',
                campaign_data: {
                  id: item._id,
                  category: item.category,
                  token: e,
                },
              });
              navigation.navigate('DonatePage');
              dispatch({type: 'fundraise', fundraise: false});
            })
            .catch((e) => {});
        }}
      />
    );
  }
  useEffect(() => {
    axios({
      method: 'GET',
      url: `https://talikasih.kuyrek.com:3001/campaign/category?category=${reducer.category}&page=1&limit=10`,
    })
      .then((e) => {
        setSearchResult(e.data.data);
      })
      .catch((e) => {
        console.log(e);
        alert(e);
      });
  }, [reducer.category]);
  useEffect(() => {
    axios({
      method: 'GET',
      url: API_NO_PARAM_CONFIG.urgent,
    })
      .then((e) => {
        getUrgent(e.data.data);
      })
      .catch((e) => console.log(e));

    axios({
      method: 'GET',
      url: API_NO_PARAM_CONFIG.newest,
    })
      .then((e) => {
        setNewest(e.data.data);
      })
      .catch((e) => console.log(e));

    axios({
      method: 'GET',
      url: API_NO_PARAM_CONFIG.popular,
    })
      .then((e) => {
        setPopular(e.data.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      })
      .catch((e) => console.log(e));
  }, []);

  const dispatch = useDispatch();
  const renderItem2 = ({item}) => {
    return (
      <CampaignFiltered
        image={item.images}
        title={item.title}
        category={item.category}
        progress={item.total_donation_rupiah / item.goal}
        user={item.user.name}
        raised={amountParser(item.total_donation_rupiah)}
        goal={amountParser(item.goal)}
        onPress={() => {
          dispatch({
            type: 'CAMPAIGN_DATA_REQUIRED',
            campaign_data: {
              id: item._id,
              category: item.category,
            },
          });

          navigation.navigate('DonatePage');
        }}
      />
    );
  };

  function Home() {
    return (
      <View>
        <View>
          <Carousel
            data={data}
            renderItem={renderItemCarousel}
            sliderWidth={ScreenWidth}
            itemWidth={ScreenWidth - 32}
            layout="default"
          />
        </View>
        <View style={{paddingLeft: 16, paddingRight: 16}}>
          <Text
            style={{
              fontWeight: 'bold',
              marginBottom: 10,
              textDecorationLine: 'underline',
              fontSize: 16,
            }}>
            Newest
          </Text>
          <FlatList
            data={newest}
            keyExtractor={newest._id}
            renderItem={renderItem}
            showsHorizontalScrollIndicator={false}
            horizontal
          />
          <Text
            style={{
              fontWeight: 'bold',
              marginBottom: 10,
              textDecorationLine: 'underline',
              fontSize: 16,
            }}>
            Most Urgent
          </Text>
          <FlatList
            data={urgent}
            showsHorizontalScrollIndicator={false}
            keyExtractor={urgent._id}
            renderItem={renderItem}
            horizontal
          />
          <Text
            style={{
              fontWeight: 'bold',
              marginBottom: 10,
              textDecorationLine: 'underline',
              fontSize: 16,
            }}>
            Gained Momentum
          </Text>
          <FlatList
            data={popular}
            renderItem={renderItem}
            keyExtractor={popular._id}
            horizontal
            showsHorizontalScrollIndicator={false}
          />
        </View>
      </View>
    );
  }

  function Filtered() {
    return (
      <View>
        <View style={{paddingHorizontal: 16}}>
          <Text style={{marginTop: 15, color: '#A43F3C'}}>
            {reducer.category}
          </Text>
          <Text style={{fontSize: 25, marginBottom: 20}}>
            Help Them With What They Need
          </Text>
        </View>
        <View style={{alignItems: 'center'}}>
          <FlatList renderItem={renderItem2} data={searchResult} />
        </View>
      </View>
    );
  }

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <View>
        <TopBar
          nav={navigation}
          onPress={() => {
            dispatch({type: 'Filter', payloadFilter: false});
          }}
        />
      </View>
      <ScrollView>
        {isLoading ? <Placeholder /> : filter ? <Filtered /> : <Home />}
      </ScrollView>

      <View
        style={{
          position: 'absolute',
          zIndex: 1,
          justifyContent: 'flex-end',
          alignSelf: 'flex-end',
          paddingRight: 16,
          marginTop: ScreenHeight - 200,
          height: 100,
          alignItems: 'flex-end',
        }}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            navigation.navigate('Filter');
          }}>
          <View
            style={{
              padding: 16,
              backgroundColor: '#F4F4F4',
              elevation: 4,
              borderRadius: 12,
            }}>
            <Icon name="filter-alt" size={32} color="#1D94A8" />
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}
