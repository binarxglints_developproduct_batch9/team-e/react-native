import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  TextInput,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/Feather';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch} from 'react-redux';
import LottieView from 'lottie-react-native';
const ScreenWidth = Dimensions.get('window').width;

export default function loginPage({navigation}) {
  const [disabled, setDisabled] = useState(true);
  const [disabledPassword, setDisabledPassword] = useState(true);
  const [isValidUser, setIsValidUser] = useState(true);
  const [isValidPassword, setIsValidPassword] = useState(true);
  const [Loading, setLoading] = useState(false);
  const [Nama, setNama] = useState(null);
  const [Email, setEmail] = useState(null);
  const [Password, setPassword] = useState(null);
  const [RepPass, setRepPass] = useState(null);
  const [Visibility, setVisibility] = useState(false);
  const [Visibility2, setVisibility2] = useState(false);
  const [secure2, setSecure2] = useState(true);
  const [secure, setSecure] = useState(true);
  let nameInput = null;
  let emailInput = null;
  let passwordInput = null;
  let passwordConfirmInput = null;
  const dispatch = useDispatch();
  const [passwordConfirm, setpasswordConfirm] = useState(false);
  const backButton = () => {
    navigation.navigate('BottomTabbed', {screen: 'Donate'});
  };
  BackHandler.addEventListener('hardwareBackPress', backButton);
  useEffect(() => {
    BackHandler.removeEventListener('hardwareBackPress', backButton);
  }, [backButton]);
  const passwordConfirmation = (password) => {
    if (Password == password) {
      setDisabledPassword(false);
      setpasswordConfirm(false);
    } else {
      setDisabledPassword(true);
      setpasswordConfirm(true);
    }
  };
  const passwordValidation = (password) => {
    if (password.length >= 8) {
      setIsValidPassword(true);
    } else {
      setIsValidPassword(false);
    }
  };
  const isEmpty = (input) => {
    if (input === '') {
      setIsValidUser(false);
      setDisabled(true);
    } else if (input.length < 14) {
      setIsValidUser(false);
      setDisabled(true);
    } else if (input.length >= 14) {
      setIsValidUser(true);
      setDisabled(false);
    }
  };
  const LoadingView = () => {
    return (
      <View
        style={{
          width: ScreenWidth,
          height: '100%',
        }}>
        <LottieView
          source={require('../../Asset/JSON Animation/lf30_editor_bozdlxrs.json')}
          autoPlay
          loop
          style={{
            flex: 1,
          }}
        />
      </View>
    );
  };
  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={{
        flex: 1,
      }}>
      <View style={{flex: 1, backgroundColor: '#f4f4f4', alignItems: 'center'}}>
        <View style={{alignSelf: 'flex-start', marginTop: 20, marginLeft: 20}}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.goBack()}>
            <Icon name="keyboard-backspace" size={30} color="#1D94A8" />
          </TouchableOpacity>
        </View>

        <Image
          style={{
            aspectRatio: 1,
            height: ScreenWidth - 180,
            width: ScreenWidth - 172,
            marginTop: 8,
          }}
          source={require('../../Asset/Logo2.png')}
        />
        <View style={styles.textinput}>
          <TextInput
            ref={(input) => {
              input = nameInput;
            }}
            placeholder="Name"
            onChangeText={(nama) => setNama(nama)}
            style={{
              flex: 1,
            }}
            onSubmitEditing={() => emailInput.focus()}
          />
        </View>
        {!isValidUser && (
          <View
            style={{
              alignSelf: 'flex-start',
              marginHorizontal: 32,
            }}>
            <Text style={{color: '#A43F3C'}}>Please Input Valid Email</Text>
          </View>
        )}

        <View style={styles.textinput}>
          <TextInput
            ref={(input) => {
              emailInput = input;
            }}
            onSubmitEditing={() => passwordInput.focus()}
            onEndEditing={(e) => isEmpty(e.nativeEvent.text)}
            placeholder="Email"
            onChangeText={(email) => setEmail(email)}
          />
        </View>
        {!isValidUser && (
          <View
            style={{
              alignSelf: 'flex-start',
              marginHorizontal: 32,
            }}>
            <Text style={{color: '#A43F3C'}}>Please Input Valid Email</Text>
          </View>
        )}
        <View style={styles.textinput}>
          <TextInput
            ref={(input) => {
              passwordInput = input;
            }}
            onEndEditing={(e) => passwordValidation(e.nativeEvent.text)}
            onSubmitEditing={() => passwordConfirmInput.focus()}
            placeholder="Password"
            secureTextEntry={secure2}
            onChangeText={(password) => setPassword(password)}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                setVisibility2(Visibility2 ? false : true);
                setSecure2(secure2 ? false : true);
              }}>
              <Icons
                name={!Visibility2 ? 'eye' : 'eye-off'}
                size={24}
                color="#9f9f9f"
              />
            </TouchableOpacity>
          </View>
        </View>
        {!isValidPassword && (
          <View
            style={{
              alignSelf: 'flex-start',
              marginHorizontal: 32,
            }}>
            <Text style={{color: '#A43F3C', fontSize: 12}}>
              Password Length Must Be 8 Characters or more
            </Text>
          </View>
        )}
        <View style={styles.textinput}>
          <TextInput
            ref={(input) => {
              passwordConfirmInput = input;
            }}
            placeholder="Confirm Password"
            secureTextEntry={secure}
            onChangeText={(passwordRepeat) => setRepPass(passwordRepeat)}
            onEndEditing={(e) => passwordConfirmation(e.nativeEvent.text)}
          />
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                setVisibility(Visibility ? false : true);
                setSecure(secure ? false : true);
              }}>
              <Icons
                name={!Visibility ? 'eye' : 'eye-off'}
                size={24}
                color="#9f9f9f"
              />
            </TouchableOpacity>
          </View>
        </View>
        {passwordConfirm && (
          <View
            style={{
              alignSelf: 'flex-start',
              marginHorizontal: 32,
            }}>
            <Text style={{color: '#A43F3C', fontSize: 12}}>
              Password Doesnt Match
            </Text>
          </View>
        )}
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            disabled={
              (disabled, disabledPassword, passwordConfirm ? true : false)
            }
            style={styles.button}
            onPress={() => {
              setLoading(true);
              axios({
                method: 'POST',
                url: 'https://talikasih.kuyrek.com:3000/user/signup',
                data: {
                  name: Nama,
                  email: Email,
                  password: Password,
                  passwordConfirmation: RepPass,
                },
              })
                .then(async (e) => {
                  await AsyncStorage.setItem(
                    'token',
                    JSON.stringify(e.data.token),
                    await axios({
                      method: 'GET',
                      url: `https://talikasih.kuyrek.com:3000/user/authorization`,
                      headers: {
                        Authorization: `Bearer ${e.data.token}`,
                      },
                    })
                      .then(async (res) => {
                        await AsyncStorage.setItem('userID', res.data.user.id);

                        dispatch({
                          type: 'userID',
                          payloadUserID: res.data.user.id,
                        });
                        dispatch({
                          type: 'status',
                          status: true,
                        });
                        setTimeout(() => {
                          setLoading(false);
                          navigation.navigate('BottomTabbed', {
                            screen: 'Donate',
                          });
                        }, 3000);
                      })
                      .catch((e) => {
                        setTimeout(() => {
                          setLoading(false);
                          alert(e);
                        }, 3000);
                      }),
                  );
                  setTimeout(() => {
                    setLoading(false);
                    alert(e.data.message);
                    navigation.navigate('BottomTabbed');
                  }, 3000);
                })
                .catch((e) => {
                  setTimeout(() => {
                    setLoading(false);
                    alert('error', e);
                  }, 3000);
                });
            }}>
            <Text
              style={{
                color: 'white',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              SIGN UP
            </Text>
          </TouchableOpacity>
          {Loading && <LoadingView />}
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <Text>Already have an account? </Text>
          <TouchableOpacity>
            <Text
              style={{
                color: '#1D94A8',
                borderBottomColor: '#1d94a8',
                borderBottomWidth: 1,
              }}
              onPress={() => {
                navigation.navigate('Login');
              }}>
              Sign In
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
}

var styles = StyleSheet.create({
  textinput: {
    flexDirection: 'row',
    width: ScreenWidth - 64,
    alignItems: 'center',
    backgroundColor: '#FCFCFC',
    marginHorizontal: 55,
    borderBottomWidth: 1,
    marginTop: 16,
    paddingHorizontal: 10,
    paddingVertical: 2,
    borderBottomColor: '#9f9f9f',
  },
  button: {
    alignItems: 'center',
    marginTop: 30,
    width: ScreenWidth - 64,
    backgroundColor: '#A43F3C',
    paddingVertical: 12,
    borderRadius: 3,
  },
});
