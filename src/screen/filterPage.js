import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Touchable,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import TopBarPage from '../component/topBarPage';
import BottomButton from '../component/bottomButton';
import {FlatList, ScrollView} from 'react-native-gesture-handler';
import {
  Section1,
  Section2,
  Section3,
  Section4,
} from '../component/sectionFilterButton';
const ScreenWidth = Dimensions.get('window').width;
const data = [
  {
    name: 'Disability',
    path: require('../../Asset/Disability.png'),
  },
  {
    name: 'Medical',
    path: require('../../Asset/Medical.png'),
  },
  {
    name: 'Education',
    path: require('../../Asset/Education.png'),
  },
  {
    name: 'Religious',
    path: require('../../Asset/Religious.png'),
  },
  {
    name: 'Humanity',
    path: require('../../Asset/Humanity.png'),
  },
  {
    name: 'Environment',
    path: require('../../Asset/Enviroment.png'),
  },
  {
    name: 'Disaster',
    path: require('../../Asset/Disastah.png'),
  },
  {
    name: 'Sociopreneur',
    path: require('../../Asset/Socioprenour.png'),
  },
];
const Item = ({title, path, onPress}) => {
  return (
    <TouchableOpacity
      
      activeOpacity={0.8}
      onPress={onPress}
      style={{
        marginHorizontal: 4,
        justifyContent: 'center',
        marginTop: 15,
        alignItems: 'center',
        flex: 2,
        borderWidth: 1,
        elevation: 6,
        backgroundColor: '#fff',
        borderRadius: 5,
        borderColor: '#E1E0E0',
        height: 80,
      }}>
      <Image
        style={{
          resizeMode: 'contain',
          width: 40,
          height: 40,
        }}
        source={path}
      />
      <Text
        style={{
          fontSize: 10,
          letterSpacing: 0.5,
          color: '#9F9F9F',
          marginTop: 10,
        }}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};
export default function filterPage({navigation}) {
  const [category, setCategory] = useState('');
  function renderItem({item}) {
    return (
      <Item
        title={item.name}
        path={item.path}
        onPress={() => setCategory(item.name)}
      />
    );
  }

  const [clicked, setClicked] = useState(false);
  const [clicked1, setClicked1] = useState(false);
  const [clicked2, setClicked2] = useState(false);
  const [clicked3, setClicked3] = useState(false);
  const dispatch = useDispatch();

  return (
    <View style={{flex: 1}}>
      <TopBarPage title="Filter and Sort" nav={navigation} />
      <ScrollView style={{flex: 1, backgroundColor: '#fff'}}>
        <View
          style={{
            paddingHorizontal: 16,
            flex: 1,
          }}>
          <Text style={{fontSize: 14, color: '#9f9f9f', marginTop: 16}}>
            Filter by category
          </Text>
          <FlatList
            data={data}
            renderItem={renderItem}
            numColumns={4}
            keyExtractor={data.name}
          />
          <View>
            <Text style={{fontSize: 14, color: '#9f9f9f', marginTop: 24}}>
              Sort by
            </Text>
          </View>
          <View>
            <Section1
              title="Newest"
              clicked={clicked}
              onPress={() => {
                setClicked1(false);
                setClicked(true);
                setClicked2(false);
                setClicked3(false);
              }}
            />
            <Section2
              title="Most Urgent"
              clicked={clicked1}
              onPress={() => {
                setClicked1(true);
                setClicked(false);
                setClicked2(false);
                setClicked3(false);
              }}
            />
            <Section3
              title="Popular"
              clicked={clicked2}
              onPress={() => {
                setClicked1(false);
                setClicked(false);
                setClicked2(true);
                setClicked3(false);
              }}
            />
            <Section4
              title="Less Donation"
              clicked={clicked3}
              onPress={() => {
                setClicked1(false);
                setClicked(false);
                setClicked2(false);
                setClicked3(true);
              }}
            />
          </View>
        </View>
      </ScrollView>
      <View style={{}}>
        <BottomButton
          title="Filter"
          nav={navigation}
          onPress={() => {
            navigation.navigate('BottomTabbed'), {screen: 'Donate'};
            dispatch({
              type: 'Filter',
              payloadFilter: true,
            });
            dispatch({
              type: 'category',
              payloadCategory: category,
            });
          }}
        />
      </View>
    </View>
  );
}
