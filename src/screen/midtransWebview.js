import React, {useEffect, useState} from 'react';
import {View, Text, BackHandler} from 'react-native';
import {WebView} from 'react-native-webview';
import {useSelector} from 'react-redux';
import LottieView from 'lottie-react-native';

export default function midtransWebview({navigation}) {
  const [isLoading, setIsLoading] = useState(true);
  const backButton = () => {
    navigation.navigate('BottomTabbed');
  };
  BackHandler.addEventListener('hardwareBackPress', backButton);
  const midtransData = useSelector((state) => state.midtransData);
  useEffect(() => {
    midtransData !== [] && setIsLoading(false);
  }, [midtransData]);
  useEffect(() => {
    BackHandler.removeEventListener('hardwareBackPress', backButton);
  }, [backButton]);
  function LoadingScreen() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: 200,
            height: 200,
          }}>
          <LottieView
            source={require('../../Asset/JSON Animation/loading.json')}
            loop
            autoPlay
          />
        </View>
      </View>
    );
  }
  return (
    <View
      style={{
        flex: 1,
      }}>
      {isLoading ? (
        <LoadingScreen />
      ) : (
        <WebView source={{uri: midtransData.redirect_url}} />
      )}
    </View>
  );
}
