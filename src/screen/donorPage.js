import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Modal,
  Image,
  ToastAndroid,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

// Import Component
import TopBarPage from '../component/topBarPage';
import DonorInput from '../component/donorInput';
import PaymentCard from '../component/paymentCard';
import BottomButton from '../component/bottomButton';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Import Component
import amountParser from '../component/amountParser';
const screenWidth = Dimensions.get('window').width;

function Credit() {
  return (
    <View
      style={{
        marginBottom: 32,
        flex: 1,
      }}>
      {/* <DonorInput title="Card Number" />
      <View
        style={{
          flexDirection: 'row',
          marginTop: 24,
        }}>
        <View
          style={{
            marginRight: 24,
          }}>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontWeight: 'bold',
              }}>
              Expiry Date
            </Text>
            <Text
              style={{
                color: '#A43F3C',
              }}>
              *
            </Text>
          </View>
          <TextInput
            maxLength={4}
            keyboardType="numeric"
            style={{
              width: screenWidth - 222,
              backgroundColor: '#fcfcfc',
              borderBottomColor: '#9f9f9f',
              borderBottomWidth: 2,
              paddingHorizontal: 16,
              height: 48,
              alignSelf: 'flex-start',
            }}
          />
        </View>
        <View>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text
              style={{
                fontWeight: 'bold',
              }}>
              CVV
            </Text>
            <Text
              style={{
                color: '#A43F3C',
              }}>
              *
            </Text>
          </View>
          <TextInput
            maxLength={3}
            keyboardType="numeric"
            style={{
              width: screenWidth - 222,
              backgroundColor: '#fcfcfc',
              borderBottomColor: '#9f9f9f',
              borderBottomWidth: 2,
              paddingHorizontal: 16,
              height: 48,
              alignSelf: 'flex-start',
            }}
          />
        </View>
      </View> */}
    </View>
  );
}

export default function donorPage({navigation}) {
  const dispatch = useDispatch();
  const [isVisible, setIsVisible] = useState(false);
  const [Expired, setExpired] = useState('');
  const [story, setStory] = useState('');
  const [editable, setEditable] = useState(true);
  const [name, setName] = useState('');
  const [anonymous, setAnonymous] = useState(false);
  const [focus, setFocus] = useState(false);
  const [CC, setCC] = useState(true);
  const [amount, setAmount] = useState(0);
  const [image, setImage] = useState(null);
  const donation_data = useSelector((state) => state.donation_data);
  const response = useSelector((state) => state.response);
  const [responseFromSaga, setResponseFromSaga] = useState(false);
  useEffect(() => {
    setResponseFromSaga(response);
  }, [response]);
  function handleInput(text) {
    let texter = text.split(' ').join('');
    if (texter.length > 0) {
      texter = texter.match(new RegExp('.{1,2}', 'g')).join(' ');
    }
    setExpired(texter);
  }

  function Bank() {
    return (
      <View
        style={{
          width: screenWidth - 32,
          backgroundColor: '#f4f4f4',
          paddingTop: 16,
          paddingBottom: 36,
          paddingHorizontal: 16,
        }}>
        <Text style={{fontWeight: 'bold', color: '#1D94A8', fontSize: 16}}>
          Transfer To
        </Text>
        <View
          style={{
            marginVertical: 16,
          }}>
          <Text style={{color: '#9f9f9f'}}>Account Number</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{fontWeight: 'bold', fontSize: 16}}>
              1234 5678 90{' '}
            </Text>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() =>
                  ToastAndroid.show('COPIED TO CLIPBOARD', ToastAndroid.SHORT)
                }>
                <Text style={{fontWeight: 'bold', textTransform: 'uppercase'}}>
                  COPy
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            marginBottom: 16,
          }}>
          <Text
            style={{
              color: '#9f9f9f',
            }}>
            Account Holder Name
          </Text>
          <Text
            style={{
              fontWeight: 'bold',
            }}>
            TaliKasih
          </Text>
        </View>

        <View>
          <Text style={{color: '#9f9f9f'}}>Total Amount</Text>
          <View style={{flexDirection: 'row'}}>
            <Text style={{flex: 1, fontWeight: 'bold', fontSize: 16}}>
              {amount === 0 || null ? 'IDR 20.0000.000' : `IDR` + ' ' + amount}
            </Text>
            <TouchableOpacity
              style={{flex: 1, alignItems: 'flex-end'}}
              onPress={() =>
                ToastAndroid.show('COPIED TO CLIPBOARD', ToastAndroid.SHORT)
              }>
              <Text style={{fontWeight: 'bold', textTransform: 'uppercase'}}>
                Copy
              </Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            onPress={() => {
              launchImageLibrary(
                {
                  mediaType: 'photo',
                  maxHeight: 500,
                  maxWidth: 500,
                },
                (response) => {
                  setImage(response);
                },
              );
            }}
            style={{
              marginTop: 16,
              paddingVertical: 8,
              paddingHorizontal: 0,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                color: '#A43F3C',
              }}>
              Add Transaction Slip (JPEG)
            </Text>
          </TouchableOpacity>
          {image !== null && (
            <Image
              source={image}
              style={{width: screenWidth - 200, height: 200}}
            />
          )}
        </View>
      </View>
    );
  }
  function Midtrans(name, amount, id, message, navigation, visible) {
    const token = AsyncStorage.getItem('token');
    token.then((e) => {
      dispatch({
        type: 'CREATE_DONATION_REQUESTED',
        payload: {
          name,
          amount,
          id,
          message,
          token: e,
          navigation,
          visible,
        },
      });
    });
  }
  function BankPayment(visible) {
    const token = AsyncStorage.getItem('token');
    token.then((e) => {
      dispatch({
        type: 'CREATE_TRANSFER_DONATION_REQUESTED',
        payload: {
          name,
          amount,
          id: donation_data.id,
          message: story,
          token: e,
          image,
          navigation,
          visible,
        },
      });
    });
  }
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <TopBarPage title="Donation" nav={navigation} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            paddingHorizontal: 16,
            paddingVertical: 24,
          }}>
          <View>
            <DonorInput
              keyboard="number-pad"
              placeholder="e.g 20.000.000"
              title="Amount"
              onChangeText={(total) => setAmount(total)}
            />
          </View>
          <View
            style={{
              marginTop: 16,
              marginBottom: 4,
            }}>
            <DonorInput
              keyboard="default"
              placeholder={anonymous ? 'anonymous' : donation_data.Name}
              title="Name"
              editable={editable}
              onChangeText={(text) => setName(text)}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 24,
            }}>
            <CheckBox
              onValueChange={() => {
                setAnonymous(anonymous ? false : true);
                setName('anonymous');
                setEditable(editable ? false : true);
              }}
              value={anonymous}
              tintColor="#f4f4f4"
              onFillColor="#1D94A8"
            />
            <Text>Anonymous</Text>
          </View>
          <View
            style={{
              marginBottom: 24,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: 'bold',
                  marginBottom: 16,
                }}>
                Message
              </Text>
              <Text
                style={{
                  color: '#A43F3C',
                }}>
                *
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'flex-start',
                height: 192,
                backgroundColor: '#fcfcfc',
                borderBottomColor: '#9f9f9f',
                borderBottomWidth: 2,
              }}>
              <AutoGrowingTextInput
                placeholder="Give Them Support..."
                onChangeText={(text) => setStory(text)}
                style={{
                  padding: 16,
                  margin: 0,
                  justifyContent: 'flex-start',
                }}
              />
            </View>
          </View>
          <View
            style={{
              marginBottom: 32,
            }}>
            <View
              style={{
                flexDirection: 'row',
                marginBottom: 8,
              }}>
              <Text
                style={{
                  fontWeight: 'bold',
                }}>
                Select Payment
              </Text>
              <Text
                style={{
                  color: '#A43F3C',
                }}>
                *
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                flex: 1,
              }}>
              <View
                style={{
                  marginRight: 24,
                  flex: 1,
                }}>
                <PaymentCard
                  Title="Midtrans"
                  iconName="credit-card"
                  focus={focus}
                  onPress={() => {
                    setFocus(focus ? false : true);
                    setCC(CC ? false : true);
                  }}
                />
              </View>
              <View
                style={{
                  flex: 1,
                }}>
                <PaymentCard
                  Title="Bank Transfer"
                  iconName="account-balance"
                  focus={!focus}
                  onPress={() => {
                    setFocus(focus ? false : true);
                    setCC(CC ? false : true);
                  }}
                />
              </View>
            </View>
          </View>
          {CC ? <Credit /> : <Bank />}
        </View>
        <Modal visible={isVisible} transparent={true}>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                elevation: 2,
                borderRadius: 6,
                padding: 16,
                backgroundColor: '#fff',
                alignItems: 'center',
              }}>
              <View style={{}}>
                <Image
                  source={require('../../Asset/donation-suceed-message.png')}
                  style={{
                    width: screenWidth - 260,
                    height: screenWidth - 260,
                    marginHorizontal: 48,
                  }}
                />
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'flex-end',
                    flex: 1,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      setIsVisible(false);
                      dispatch({
                        type: 'CREATE_CAMPAIGN_RESPONSE',
                        payloadResponse: true,
                      });

                      navigation.navigate('DonatePage');
                    }}>
                    <Icon name="close" color="#000" size={24} />
                  </TouchableOpacity>
                </View>
              </View>
              <View
                style={{
                  marginTop: 8,
                  alignItems: 'center',
                  paddingBottom: 8,
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    letterSpacing: 0.9,
                    textTransform: 'uppercase',
                  }}>
                  Thank You!
                </Text>
                <Text>Wait For Your Donation Getting Verified</Text>
              </View>
            </View>
          </View>
        </Modal>
      </ScrollView>
      <BottomButton
        title="Donate"
        onPress={() => {
          CC
            ? Midtrans(
                name,
                amount,
                donation_data.id,
                story,
                navigation,
                setIsVisible,
              )
            : BankPayment(setIsVisible);
        }}
      />
    </View>
  );
}
