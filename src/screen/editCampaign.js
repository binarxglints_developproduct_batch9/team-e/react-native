import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  TextInput,
  ScrollView,
} from 'react-native';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import Icon from 'react-native-vector-icons/Feather';
import {Picker} from '@react-native-picker/picker';
import DatePicker from '@react-native-community/datetimepicker';
import {launchImageLibrary} from 'react-native-image-picker';

// Import Component
import TopBar from '../component/topBar';
import Input from '../component/donorInput';
import {useDispatch, useSelector} from 'react-redux';

const screenWidth = Dimensions.get('window').width;
const NoResponse = () => {
  return (
    <View
      style={{
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Icon name="plus-circle" color="#9f9f9f" size={32} />
      <Text
        style={{
          color: '#9f9f9f',
          fontSize: 16,
          marginTop: 8,
        }}>
        Add Header Photo
      </Text>
    </View>
  );
};

export default function editCampaign() {
  const dispatch = useDispatch();
  const donation_data = useSelector((state) => state.donation_data);
  const [showData, setShowData] = useState(false);
  const [year, setYear] = useState(2021);
  const [months, setMonths] = useState(null);
  const [isValidYear, setIsValidYear] = useState(false);
  const [response, setResponse] = useState('');
  const [title, setTitle] = useState('');
  const [goal, setGoal] = useState(0);
  const [category, setCategory] = useState('Disability');
  const [story, setStory] = useState('');
  const [isValidMonth, setIsValidMonth] = useState(false);
  const [isDayValid, setIsDayValid] = useState(false);
  const [days, setDays] = useState(null);
  let yearRef = null;
  let monthRef = null;
  let dayReff = null;
  const dueDate = `${year}-${months}-${days}`;
  const yearValidation = (year) => {
    if (year >= 2021 && year < 2025) {
      setIsValidYear(true);
    } else {
      setIsValidYear(false);
    }
  };
  const monthValidation = (month) => {
    if (month <= 12 && month >= 1) {
      setIsValidMonth(true);
    } else {
      setIsValidMonth(false);
    }
  };
  const dayValidation = (day) => {
    if (day >= 1 && day <= 31) {
      setIsDayValid(true);
    } else {
      setIsDayValid(false);
    }
  };
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <TopBar />
      {/* Component Goes Here */}
      <ScrollView>
        <View
          style={{
            alignItems: 'center',
            marginTop: 24,
            flex: 1,
            paddingHorizontal: 16,
            paddingBottom: 32,
          }}>
          <TouchableOpacity
            onPress={() =>
              launchImageLibrary(
                {
                  mediaType: 'photo',
                  includeBase64: false,
                  maxHeight: 200,
                  maxWidth: 200,
                },
                (resp) => setResponse(resp),
              )
            }>
            <View
              style={{
                backgroundColor: response === '' ? '#E1E0E0' : 'transparent',
                marginBottom: 32,
                width: screenWidth - 42,
                height: 200,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              {response == '' ? <NoResponse /> : console.log('')}
              <Image
                source={response}
                style={{
                  width: response !== '' ? screenWidth - 42 : 0,
                  height: response !== '' ? 200 : 0,
                }}
              />
            </View>
          </TouchableOpacity>
          <View
            style={{
              marginBottom: 16,
            }}>
            <Input
              title="Title"
              placeholder="e.h Help us to get clean water"
              onChangeText={(t) => setTitle(t)}
            />
          </View>
          <View
            style={{
              marginBottom: 16,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text style={{fontWeight: 'bold'}}>Category</Text>
              <Text style={{color: '#A43F3C'}}>*</Text>
            </View>
            <View
              style={{
                borderBottomColor: '#9f9f9f',
                borderBottomWidth: 2,
              }}>
              <Picker
                mode="dropdown"
                selectedValue={category}
                onValueChange={(value) => setCategory(value)}
                style={{
                  width: screenWidth - 32,
                  backgroundColor: '#fcfcfc',
                  height: 48,
                }}>
                <Picker.Item label="Disability" value="Disability" />
                <Picker.Item label="Medical" value="Medical" />
                <Picker.Item label="Religion" value="Religion" />
                <Picker.Item label="Education" value="Education" />
                <Picker.Item label="Humanity" value="Humanity" />
                <Picker.Item label="Environment" value="Environment" />
                <Picker.Item label="Disaster" value="Disaster" />
                <Picker.Item label="SocioPreuner" value="SocioPreuner" />
              </Picker>
            </View>
          </View>
          <View
            style={{
              marginBottom: 16,
            }}>
            <Input
              title="Goal"
              keyboard="number-pad"
              placeholder="e.h Help us to get clean water"
              onChangeText={(g) => setGoal(g)}
            />
          </View>

          <View
            style={{
              marginBottom: 16,
              width: screenWidth - 32,
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text style={{fontWeight: 'bold'}}>Due Date</Text>
              <Text style={{color: '#A43F3C'}}>*</Text>
            </View>
            <View style={{flexDirection: 'row', flex: 6}}>
              <TextInput
                ref={(input) => {
                  input = yearRef;
                }}
                placeholder="YYYY"
                maxLength={4}
                keyboardType="number-pad"
                onChangeText={(e) => setYear(e)}
                onEndEditing={(e) => {
                  yearValidation(e.nativeEvent.text);
                }}
                onSubmitEditing={() => monthRef.focus()}
                style={{
                  height: 48,
                  flex: 2,
                  paddingHorizontal: 16,
                  backgroundColor: '#fcfcfc',
                  borderBottomWidth: 2,
                  borderBottomColor: isValidYear ? '#9f9f9f' : '#A43F3C',
                  alignItems: 'center',
                }}
              />

              <TextInput
                placeholder="MM"
                ref={(input) => {
                  monthRef = input;
                }}
                maxLength={2}
                keyboardType="number-pad"
                onChangeText={(e) => {
                  setMonths(e);
                }}
                onEndEditing={(e) => monthValidation(e.nativeEvent.text)}
                onSubmitEditing={() => dayReff.focus()}
                style={{
                  height: 48,
                  flex: 2,
                  paddingHorizontal: 16,
                  backgroundColor: '#fcfcfc',
                  borderBottomWidth: 2,
                  borderBottomColor: isValidMonth ? '#9f9f9f' : '#A43F3C',
                  marginHorizontal: 24,
                }}
              />

              <TextInput
                ref={(input) => {
                  dayReff = input;
                }}
                placeholder="DD"
                maxLength={2}
                keyboardType="number-pad"
                onChangeText={(e) => setDays(e)}
                onEndEditing={(e) => dayValidation(e.nativeEvent.text)}
                style={{
                  flex: 2,
                  height: 48,
                  paddingHorizontal: 16,
                  backgroundColor: '#fcfcfc',
                  borderBottomWidth: 2,
                  borderBottomColor: isDayValid ? '#9f9f9f' : '#A43F3C',
                }}
              />
            </View>
            
          </View>
          <View
            style={{
              alignSelf: 'flex-start',
            }}>
            <View
              style={{
                flexDirection: 'row',
              }}>
              <Text style={{fontWeight: 'bold'}}>Story</Text>
              <Text style={{color: '#A43F3C'}}>*</Text>
            </View>
            <View
              style={{
                height: 328,
                marginTop: 16,
              }}>
              <AutoGrowingTextInput
                onChangeText={(s) => setStory(s)}
                placeholder="Tell your story..."
                style={{
                  width: screenWidth - 32,
                  backgroundColor: '#fcfcfc',
                  padding: 16,
                }}
              />
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              dispatch({
                type: 'EDIT_CAMPAIGN_REQUESTED',
                payload: {
                  token: donation_data.token,
                  id: donation_data.id,
                  title,
                  goal,
                  category,
                  story,
                  due_date: dueDate,
                },
              });
            }}
            style={{
              width: screenWidth - 32,
              borderRadius: 6,
              marginTop: 24,
              alignItems: 'center',
              paddingVertical: 16,
              backgroundColor: '#A43F3C',
            }}>
            <Text
              style={{
                fontSize: 16,
                color: '#fff',
                fontWeight: 'bold',
                textTransform: 'uppercase',
              }}>
              Edit Campaign
            </Text>
          </TouchableOpacity>
        </View>
        {/* {showData && (
          <DatePicker
            value={new Date()}
            display="default"
            mode="date"
            onChange={(res) => {
              console.log(res.nativeEvent.timestamp);
              setDate(res.nativeEvent.timestamp);
              setShowData(false);
            }}
            style={{
              minWidth: screenWidth - 32,
              flex: 1,
            }}
          />
        )} */}
      </ScrollView>
    </View>
  );
}
