import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, Modal} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import LottieView from 'lottie-react-native';

import TopBar from '../component/topBar';
import {deleteToken} from '../store/action/action';

function SectionButton({title, icon, color, iconColor, onPress}) {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.3}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 16,
          paddingVertical: 16,
          borderBottomColor: '#e1e0e0',
          borderBottomWidth: 1,
        }}>
        <View>
          <Icon
            name={icon}
            size={20}
            color={iconColor !== 'logout' ? '#2d2d2d' : '#A43F3C'}
          />
        </View>
        <Text
          style={{
            fontSize: 16,
            color: '#000',
            fontWeight: '200',
            marginLeft: 8,
            color: color,
          }}>
          {title}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

export default function morePage({navigation}) {
  // console.log(data);
  const token = AsyncStorage.getItem('token');
  const [loggedIn, setLoggedIn] = useState(false);
  const [visibility, setVisibility] = useState(false);
  useEffect(() => {
    token
      .then((e) => (e !== null ? setLoggedIn(true) : setLoggedIn(false)))
      .catch((e) => {
        alert('Token Ilang');
        navigation.navigate('Login');
      });
  }, [token, loggedIn]);
  const dispatch = useDispatch();
  return (
    <View
      style={{
        backgroundColor: 'white',
        flex: 1,
      }}>
      <TopBar nav={navigation} />
      <View>
        <SectionButton title="About" icon="info" />
        <SectionButton title="Contact Us" icon="call" />
        <SectionButton title="FAQ" icon="question-answer" />
        {loggedIn && (
          <SectionButton
            title="Logout"
            icon="exit-to-app"
            color="#A43F3C"
            iconColor="logout"
            onPress={() => {
              setVisibility(true);
            }}
          />
        )}
      </View>
      <Modal visible={visibility} transparent={true}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            marginHorizontal: 32,
          }}>
          <View
            style={{
              backgroundColor: '#fff',
              elevation: 4,
              padding: 16,
              alignItems: 'center',
            }}>
            <View
              style={{
                width: 120,
                height: 120,
              }}>
              <LottieView
                source={require('../../Asset/JSON Animation/lf30_editor_p0fjmtyc.json')}
                autoPlay
                loop
              />
            </View>
            <View>
              <TouchableOpacity>
                <Text
                  style={{
                    textTransform: 'capitalize',
                  }}>
                  Are you sure want to logout?
                </Text>
              </TouchableOpacity>
              <View style={{flexDirection: 'row', marginTop: 4}}>
                <TouchableOpacity
                  style={{
                    flex: 2,
                    alignItems: 'flex-end',
                  }}
                  onPress={async () => {
                    await AsyncStorage.removeItem('userID');
                    await AsyncStorage.removeItem('token');
                    setVisibility(false);
                    navigation.navigate('Login');
                    dispatch({type: 'status', status: false});
                    dispatch({type: 'userID', payloadUserID: ''});
                    setLoggedIn(false);
                  }}>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#A43F3C',
                      fontWeight: 'bold',
                    }}>
                    YES
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    alignItems: 'flex-end',
                  }}
                  onPress={() => setVisibility(false)}>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#1D94A8',
                      fontWeight: 'bold',
                    }}>
                    NO
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}
