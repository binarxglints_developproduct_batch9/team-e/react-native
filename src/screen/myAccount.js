import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Dimensions,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';

import TopBar from '../component/topBar';
import InputProfile from '../component/inputProfile';
import Section from '../component/section';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {useSelector, useDispatch} from 'react-redux';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
// Import Component
import LoggedOut from '../component/loggedOutAnimated';

const screenWidth = Dimensions.get('window').width;

function backNumberParser(bankNum, bankName) {
  if (bankNum === null) {
    return 'Edit Your Account to Add Bank Account';
  } else {
    let val = bankNum + '0';
    let get = val.slice(-4, -1);
    return `${bankName} -  *******${get}`;
  }
}

function Test() {
  return (
    <SkeletonPlaceholder>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 24,
          marginHorizontal: 16,
        }}>
        <View
          style={{width: screenWidth - 160, height: 200, borderRadius: 10}}
        />

        <View
          style={{
            marginTop: 72,
            width: screenWidth - 24,
            height: 42,
            borderRadius: 4,
            marginBottom: 32,
          }}
        />
        <View
          style={{
            width: screenWidth - 24,
            height: 42,
            borderRadius: 4,
            marginBottom: 36,
          }}
        />
        <View
          style={{
            width: screenWidth - 24,
            height: 42,
            borderRadius: 4,
            marginBottom: 36,
          }}
        />
        <View
          style={{
            width: screenWidth - 24,
            height: 32,
            borderRadius: 4,
            marginBottom: 8,
          }}
        />
        <View
          style={{
            width: screenWidth - 24,
            height: 32,
            borderRadius: 4,
            marginBottom: 32,
          }}
        />
      </View>
    </SkeletonPlaceholder>
  );
}

export default function myAccount({navigation}) {
  const dispatch = useDispatch();
  const [tokenContainer, setTokenContainer] = useState('');
  const [userData, setUserData] = useState([]);
  const [Donation, setDonation] = useState([]);
  const [Campaign, setCampaign] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  // Reducer
  const userID = useSelector((state) => state.userID);
  const loginStatus = useSelector((state) => state.loginStatus);
  // Get Token from storage
  const token = AsyncStorage.getItem('token');
  useEffect(() => {
    token
      .then((e) => {
        setTokenContainer(e); // Set token to local state
        axios({
          method: 'GET',
          url: 'https://talikasih.kuyrek.com:3000/user/profile', // Get Profile/User Data
          headers: {
            Authorization: 'Bearer' + ' ' + e,
          },
        })
          .then((res) => {
            setUserData(res.data.data);
            dispatch({type: 'UserData', data: res.data.data}); // Store user data to reducer
          })
          .catch((err) => console.log('error' + err));
        axios({
          method: 'GET', // Get Donation BY USER
          url: `https://talikasih.kuyrek.com:3002/donation/user/?user_id=${userID}&page=1&limit=10`,
        }).then((e) => {
          setDonation(e.data.data);
        });
        axios({
          method: 'GET', // Get Campaign/Fundraise BY USER
          url: `https://talikasih.kuyrek.com:3001/campaign/user?user_id=${userID}&page=1&limit=10`,
        }).then((e) => {
          // console.log(e.data.data);
          setCampaign(e.data.data);
          setTimeout(() => {
            setIsLoading(false);
          }, 1000);
        });
      })
      .catch((e) => console.log('error get token : ', e));
  }, [loginStatus]);
  // Executed every time page refreshed

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <TopBar nav={navigation} />
      {isLoading && <Test />}
      {loginStatus == false ? ( // If there is no token, will return loggedOut Page
        <LoggedOut title="Login to Edit Profile" />
      ) : (
        <ScrollView>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 24,
              marginHorizontal: 16,
            }}>
            <View
              style={{
                width: screenWidth - 160,
                height: 200,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#575757',
                borderRadius: 10,
              }}>
              <Image
                source={{uri: userData.profile_image}}
                style={{
                  width: screenWidth - 160,
                  height: 200,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: '#575757',
                  borderRadius: 10,
                }}
              />
            </View>
            <View
              style={{
                marginTop: 8,
              }}>
              <TouchableOpacity
                onPress={() => navigation.navigate('EditProfile')}>
                <Text
                  style={{
                    color: '#1D94A8',
                    textDecorationLine: 'underline',
                    fontWeight: 'bold',
                    fontSize: 16,
                  }}>
                  Edit Profile
                </Text>
              </TouchableOpacity>
            </View>
            <View>
              <InputProfile
                value={userData.name}
                Title="Name"
                editable={false}
                colors={true}
              />
            </View>
            <View>
              <InputProfile
                value={userData.email}
                Title="Email"
                editable={false}
                colors={true}
              />
            </View>
            <View>
              <InputProfile
                value={
                  userData.bankName == ''
                    ? 'Complete Your Profile!'
                    : backNumberParser(
                        userData.bank_account_number,
                        userData.bank_name,
                      )
                }
                Title="Bank Info"
                editable={false}
                colors={true}
              />
            </View>
            <View
              style={{
                marginTop: 24,
              }}>
              <Section
                title="My Donations" // Section Button to show Donation by User
                value={Donation.length}
                onPress={() => navigation.navigate('MyDonation')}
              />
              <Section
                title="My Campaign" // Section Button to show Campaign by User
                value={Campaign.length}
                onPress={() => navigation.navigate('MyCampaign')}
              />
            </View>
          </View>
        </ScrollView>
      )}
    </View>
  );
}
