import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import LottieView from 'lottie-react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import TopBarPage from '../component/topBarPage';
import BottomButton from '../component/bottomButton';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import DonorInput from '../component/donorInput';
import {useDispatch, useSelector} from 'react-redux';
import amountParser from '../component/amountParser';
const screenWidth = Dimensions.get('window').width;

export default function updateFundraise({navigation}) {
  const donation_data = useSelector((state) => state.donation_data);
  console.log(donation_data);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isExceed, setIsExceed] = useState(false);
  const [modalError, setModalError] = useState(false);
  const [amount, setAmount] = useState(0);
  const [Story, setStory] = useState('');
  const [Update, setUpdate] = useState(false);
  function Amount({onChangeText, wallet}) {
    return (
      <View style={{marginTop: 24}}>
        <DonorInput
          value={amount}
          keyboard="number-pad"
          title="Amount"
          onChangeText={onChangeText}
        />
        {/* {isExceed && (
          <Text style={{color: 'red'}}>
            Withdrawal amount cannot exceed wallet
          </Text>
        )} */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'flex-end',
            marginTop: 8,
          }}>
          <Text style={{fontWeight: 'bold'}}>IDR {wallet}</Text>
          <Icon name="account-balance-wallet" size={32} color="#1D94A8" />
        </View>
      </View>
    );
  }

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TopBarPage title="Update Fundraise Progress" nav={navigation} />
      <View
        style={{
          paddingHorizontal: 16,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 8,
          }}>
          <CheckBox
            onFillColor="#1D94A8"
            onTintColor="#1D94A8"
            value={!Update}
            onValueChange={() => setUpdate(Update ? false : true)}
          />
          <Text style={{fontSize: 16, fontWeight: 'bold'}}>
            Recipient update
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 8,
          }}>
          <CheckBox
            onFillColor="#1D94A8"
            onTintColor="#1D94A8"
            value={Update}
            onValueChange={() => setUpdate(Update ? false : true)}
          />

          <Text style={{fontSize: 16, fontWeight: 'bold'}}>
            Fund Withdrawal
          </Text>
        </View>
        {Update && (
          <Amount
            onChangeText={(text) => setAmount(text)}
            // onSubmitEditing={() => amount > wallet && setIsExceed(true)}
            wallet={amountParser(donation_data.wallet)}
          />
        )}
        <View
          style={{
            marginTop: 24,
          }}>
          <View style={{flexDirection: 'row', marginBottom: 8}}>
            <Text style={{fontWeight: 'bold'}}>
              {Update ? 'Withdrawal Purpose' : 'Update'}
            </Text>
            <Text style={{color: '#A43F3C'}}>*</Text>
          </View>
          <View
            style={{
              justifyContent: 'flex-start',
              height: 328,
              backgroundColor: '#fcfcfc',
            }}>
            <AutoGrowingTextInput
              value={Story}
              placeholder="Tell Your Story..."
              style={{
                padding: 16,
                margin: 0,
                justifyContent: 'flex-start',
              }}
              onChangeText={(e) => {
                setStory(e);
              }}
            />
          </View>
        </View>
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'flex-end',
        }}>
        <BottomButton
          title="Update"
          onPress={() => {
            setIsLoading(true);
            !Update
              ? dispatch({
                  type: 'UPDATE_PROGRESS_REQUESTED',
                  payload: {
                    message: Story,
                    campaign_id: donation_data.id,
                    loading: setIsLoading,
                    succeed: setModalError,
                  },
                })
              : dispatch({
                  type: 'WITHDRAWAL_UPDATE_REQUESTED',
                  payload: {
                    message: Story,
                    id: donation_data.id,
                    amount: amount,
                    loading: setIsLoading,
                    succeed: setModalError,
                  },
                });
          }}
        />
      </View>
      <Modal transparent={true} visible={modalError}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
          }}>
          <View
            style={{
              width: screenWidth - 96,
              padding: 16,
              backgroundColor: '#fff',
              borderRadius: 6,
              alignItems: 'center',
            }}>
            <View
              style={{
                width: screenWidth - 144,
                height: 150,
              }}>
              <LottieView
                source={require('../../Asset/JSON Animation/update-succeed.json')}
                autoPlay
                loop
              />
            </View>
            <Text
              style={{
                marginTop: 8,
                fontWeight: 'bold',
                textTransform: 'uppercase',
              }}>
              Update Succeed!
            </Text>
            <View
              style={{
                width: screenWidth - 144,
                height: 1,
                backgroundColor: '#9f9f9f',
                marginVertical: 8,
              }}
            />
            <TouchableOpacity onPress={() => navigation.navigate('MyCampaign')}>
              <Text
                style={{
                  fontWeight: 'bold',
                  textTransform: 'uppercase',
                  color: '#1D94A8',
                }}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <Modal transparent={true} visible={isLoading}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={{
              width: screenWidth - 144,
              paddingVertical: 16,
              alignItems: 'center',
              backgroundColor: '#fff',
              borderRadius: 6,
            }}>
            <View
              style={{
                width: screenWidth - 200,
                height: 100,
              }}>
              <LottieView
                source={require('../../Asset/JSON Animation/loading.json')}
                autoPlay
                loop
              />
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}
