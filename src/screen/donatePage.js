import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Modal,
} from 'react-native';
import * as Progress from 'react-native-progress';
import LinearGradient from 'react-native-linear-gradient';
import Timeline from 'react-native-timeline-flatlist';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import {useDispatch, useSelector} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
// Import Component
import TopBar from '../component/topBar';
import Placeholder from '../component/campaignSkeletetal';
import BottomRowButton from '../component/bottomRowButton';
import ProblematicFlatlist from '../component/problematicFlatlist';
import CommentList from '../component/commentList';
import MiniCard from '../component/miniCard';
import {ShareModal, FundRaiseModal} from '../component/modal';
import dateParser from '../component/dateParser';
import amountParser from '../component/amountParser';
import DeleteModal from '../component/DeleteConfirmationModal';
import DeleteSuccessModal from '../component/DeleteSuccessModal';
import {set} from 'react-native-reanimated';
const screeenWidth = Dimensions.get('window').width;

function Fundraise({onPress}) {
  return (
    <View
      style={{alignItems: 'center', width: screeenWidth, marginVertical: 16}}>
      <TouchableOpacity onPress={onPress}>
        <Text
          style={{
            fontWeight: 'bold',
            textDecorationLine: 'underline',
            letterSpacing: 1,
            fontSize: 16,
          }}>
          Manage Campaign
        </Text>
      </TouchableOpacity>
    </View>
  );
}

function doNothing() {
  return;
}

export default function donatePage({navigation}) {
  const getData = useSelector((state) => state.campaign_data_response);

  const Comment = useSelector((state) => state.comment_data_response);
  const related = useSelector((state) => state.related_campaign_response);
  const update = useSelector((state) => state.campaign_update_response);
  const donation = useSelector((state) => state.donation_data_response);
  const loginStatus = useSelector((state) => state.loginStatus);
  const [userComment, setUserComment] = useState('');
  const [DeleteConfirmationModal, setDeleteConfirmationModal] = useState(false);
  const [SuccessModal, setSuccessModal] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const [FundVisibility, setFundVisibility] = useState(false);
  const [ShareVisibility, setShareVisibility] = useState(false);
  const [tokenData, setTokenData] = useState('');
  const fundraiser = useSelector((state) => state.fundraiser);
  const [updateData, setUpdateData] = useState([]);
  const userLoginData = useSelector((state) => state.userLoginData);
  const [create_comment_watcher, setCreate_comment_watcher] = useState(false);
  const campaign_data = useSelector((state) => state.campaign_data);

  const token = AsyncStorage.getItem('token');

  const dispatch = useDispatch();
  const _progress = getData.total_donation_rupiah / getData.goal;
  useEffect(() => {
    token
      .then((e) => {
        dispatch({
          type: 'GET_DATA_DONATE_PAGE',
          payload: {
            id: campaign_data.id,
            category: campaign_data.category,
            token: e,
            setIsLoading: setIsLoading,
            navigation,
          },
        });
      })
      .catch((e) => {
        dispatch({
          type: 'GET_DATA_DONATE_PAGE',
          payload: {
            id: campaign_data.id,
            category: campaign_data.category,
            navigation,
            token: e,
          },
        });
      });
  }, [campaign_data]);

  function renderItem({item}) {
    return (
      <MiniCard
        image={item !== [] && item.images}
        title={item.title}
        category={item.category}
        progress={item.total_donation_rupiah / item.goal}
        raised={amountParser(item.total_donation_rupiah)}
        goal={amountParser(item.goal)}
        onPress={() => {
          token
            .then((e) => {
              dispatch({
                type: 'GET_DATA_DONATE_PAGE',
                payload: {
                  id: item._id,
                  category: item.category,
                  navigation,
                  token: e,
                },
              });
              dispatch({type: 'fundraise', fundraise: false});
            })
            .catch((err) => {
              dispatch({
                type: 'GET_DATA_DONATE_PAGE',
                payload: {
                  id: item._id,
                  category: item.category,
                  navigation,
                },
              });
            });
        }}
      />
    );
  }
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <TopBar nav={navigation} />
      {isLoading ? (
        <Placeholder />
      ) : (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              alignItems: 'center',
              flex: 1,
            }}>
            <View>
              <Image
                source={{uri: getData !== [] ? getData.images : null}}
                style={{
                  width: screeenWidth,
                  height: 242,
                }}
              />
            </View>
            <View
              style={{
                marginHorizontal: 16,
                marginTop: 16,
                alignSelf: 'flex-start',
              }}>
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                }}>
                {getData !== [] ? getData.title : 'Still Loading'}
              </Text>
            </View>
            {/* Start Of Card View */}
            <View
              style={{
                width: screeenWidth - 32,
                borderWidth: 1,
                borderColor: '#F1EDE4',
                borderRadius: 5,
                paddingHorizontal: 16,
                paddingTop: 16,
                paddingBottom: 32,
                marginTop: 16,
              }}>
              <View
                style={{
                  marginBottom: 16,
                }}>
                <Text
                  style={{
                    fontSize: 24,
                    color: '#A43F3C',
                    fontWeight: 'bold',
                  }}>
                  IDR{' '}
                  {getData.total_donation_rupiah !== undefined &&
                    amountParser(getData.total_donation_rupiah)}
                </Text>
                <Text
                  style={{
                    fontSize: 14,
                  }}>
                  Raised from IDR{' '}
                  {getData.total_donation_rupiah !== undefined &&
                    amountParser(getData.goal) + ' '}
                  Goal
                </Text>
                <View
                  style={{
                    marginTop: 8,
                  }}>
                  <Progress.Bar
                    progress={_progress}
                    width={screeenWidth - 64}
                    color="#1D94A8"
                    borderWidth={0}
                    borderRadius={20}
                    unfilledColor="#F4F4F4"
                    height={10}
                  />
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                {getData.user.profile_image ===
                `https://talikasih.kuyrek.com:3000/img/null` ? (
                  <Icon name="perm-identity" size={42} />
                ) : (
                  <Image
                    source={{
                      uri: getData.user.profile_image,
                    }}
                    style={{width: 42, height: 42, borderRadius: 4}}
                  />
                )}

                <View
                  style={{
                    marginLeft: 8,
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      fontSize: 16,
                    }}>
                    {getData.user !== undefined
                      ? getData.user.name
                      : 'Still Loading'}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      color: '#9F9F9F',
                    }}>
                    Fundraiser
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 16,
                  flex: 1,
                }}>
                <View
                  style={{
                    flex: 1,
                    height: screeenWidth - 276,
                    backgroundColor: '#fff',
                    paddingHorizontal: 8,
                    elevation: 3,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 6,
                  }}>
                  <Text
                    style={{
                      fontSize: 24,
                      color: '#1D94A8',
                      marginBottom: 4,
                      marginHorizontal: 8,
                    }}>
                    {getData.total_donation == undefined
                      ? 10000
                      : getData.total_donation}
                  </Text>
                  <Text
                    style={{
                      color: '#9f9f9f',
                      fontSize: 12,
                    }}>
                    Donations
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    paddingHorizontal: 8,
                    marginHorizontal: 12,
                    height: screeenWidth - 276,
                    backgroundColor: '#fff',
                    elevation: 3,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 6,
                  }}>
                  <Text
                    style={{
                      fontSize: 24,
                      color: '#1D94A8',
                      marginBottom: 4,
                    }}>
                    {getData !== [] ? getData.total_share : 0}
                  </Text>
                  <Text
                    style={{
                      color: '#9f9f9f',
                      fontSize: 12,
                    }}>
                    Share
                  </Text>
                </View>
                <View
                  style={{
                    flex: 1,
                    paddingHorizontal: 8,
                    height: screeenWidth - 276,
                    backgroundColor: '#fff',
                    elevation: 3,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderRadius: 6,
                  }}>
                  <Text
                    style={{
                      fontSize: 24,
                      color: '#1D94A8',
                      marginBottom: 4,
                    }}>
                    {dateParser(getData !== [] ? getData.due_date : 100000)}
                  </Text>
                  <Text
                    style={{
                      color: '#9f9f9f',
                      fontSize: 12,
                    }}>
                    Days Left
                  </Text>
                </View>
              </View>
            </View>
            {/* Cards View Endpoint */}
            {fundraiser ? (
              <Fundraise
                onPress={() => {
                  setFundVisibility(true);
                }}
              />
            ) : (
              doNothing()
            )}
            {/* Start Of Story View */}
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: 32,
                paddingVertical: 16,
                marginVertical: 16,
              }}>
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                }}>
                The Story
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  letterSpacing: 0.3,
                  lineHeight: 20,
                  marginTop: 8,
                }}>
                {getData !== [] ? getData.story : 'Still Loading'}
              </Text>
            </View>
            {/* End Of Story View */}
            <LinearGradient colors={['#FAF8F3', 'transparent']}>
              <View
                style={{
                  width: screeenWidth,
                  paddingVertical: 24,
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    borderWidth: 1,
                    flex: 1,
                    borderColor: '#E1E0E0',
                    width: screeenWidth - 32,
                    backgroundColor: '#fff',
                    paddingVertical: 16,
                    paddingHorizontal: 16,
                  }}>
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: 'bold',
                      marginBottom: 16,
                    }}>
                    Updates
                  </Text>
                  <View
                    style={{
                      marginLeft: -58,
                    }}>
                    {updateData === [] ? (
                      <Timeline
                        data={updateData}
                        detailContainerStyle={{
                          paddingTop: 0,

                          justifyContent: 'flex-start',
                          top: -13,
                          marginLeft: -5,
                        }}
                        timeStyle={{
                          color: 'transparent',
                        }}
                        titleStyle={{
                          backgroundColor: '#fff',
                          marginTop: 0,
                          padding: 0,
                          fontSize: 16,
                        }}
                        descriptionStyle={{
                          justifyContent: 'flex-start',
                          padding: 16,
                          borderRadius: 6,
                          borderWidth: 1,
                          borderColor: '#E1E0E0',
                          lineHeight: 20,
                        }}
                        circleColor="#1D94A8"
                        lineColor="#1D94A8"
                      />
                    ) : (
                      <View
                        style={{
                          paddingHorizontal: 32,
                          flex: 1,
                          alignSelf: 'center',
                          width: screeenWidth - 32,
                        }}>
                        <Text
                          style={{
                            marginHorizontal: 16,
                          }}>
                          There is no update currently
                        </Text>
                      </View>
                    )}
                  </View>
                </View>
              </View>
            </LinearGradient>
            <View
              style={{
                flex: 1,
              }}>
              <View
                style={{
                  marginTop: 16,
                  paddingHorizontal: 16,
                  paddingVertical: 32,
                  borderWidth: 1,
                  flex: 1,
                  minWidth: screeenWidth - 32,
                  borderColor: '#E1E0E0',
                }}>
                <Text
                  style={{
                    fontSize: 16,
                    fontWeight: 'bold',
                    marginBottom: 16,
                    alignSelf: 'flex-start',
                    alignItems: 'center',
                    color: '#000',
                  }}>
                  Donations({donation.length})
                </Text>
                <ProblematicFlatlist Comment={donation} />
              </View>
            </View>

            <View
              style={{
                marginTop: 32,
                paddingHorizontal: 18,
                flex: 0.5,
                width: screeenWidth - 32,
                alignSelf: 'center',
                paddingVertical: 32,
                borderWidth: 1,
                borderColor: '#E1E0E0',
              }}>
              <View
                style={{
                  flex: 1,
                }}>
                <Text
                  style={{
                    fontSize: 18,
                    fontWeight: 'bold',
                    marginBottom: 16,
                  }}>
                  Comments ({Comment.length})
                </Text>

                <View
                  style={{
                    justifyContent: 'flex-start',
                    height: 144,
                    backgroundColor: 'rgba(241, 237, 228, 0.2)',
                  }}>
                  <AutoGrowingTextInput
                    placeholder="Give Them Support..."
                    onChangeText={(text) => setUserComment(text)}
                    style={{
                      padding: 16,
                      margin: 0,
                      justifyContent: 'flex-start',
                    }}
                  />
                </View>
                <View
                  style={{
                    alignItems: 'flex-end',
                  }}>
                  <TouchableOpacity
                    disabled={!loginStatus}
                    onPress={() =>
                      loginStatus
                        ? token.then((e) => {
                            dispatch({
                              type: 'CREATE_COMMENT_REQUESTED',
                              payload: {
                                token: e,
                                comment: userComment,
                                id: getData._id,
                              },
                            });
                            setCreate_comment_watcher(true);
                          })
                        : alert('Login to create comment ')
                    }
                    style={{
                      width: screeenWidth - 236,
                      height: 48,
                      backgroundColor: '#A43F3C',
                      borderRadius: 4,
                      alignItems: 'center',
                      justifyContent: 'center',
                      marginTop: 8,
                    }}
                    activeOpacity={0.8}>
                    <Text
                      style={{
                        color: '#fff',
                        fontSize: 16,
                        fontWeight: 'bold',
                        textTransform: 'uppercase',
                        letterSpacing: 1,
                      }}>
                      Post
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  // COMMENT SECTION
                  style={{
                    marginTop: 24,
                    flex: 0.5,
                  }}>
                  <CommentList data={Comment} />
                </View>
              </View>
            </View>
            <View
              style={{
                width: screeenWidth - 32,
                marginTop: 32,
                marginBottom: 24,
              }}>
              <Text
                // Related Campaign section
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  textDecorationLine: 'underline',
                  marginBottom: 16,
                }}>
                Related Campaign
              </Text>
              <View>
                <FlatList
                  showsHorizontalScrollIndicator={false}
                  horizontal
                  data={related}
                  keyExtractor={related.id}
                  renderItem={renderItem}
                />
              </View>
            </View>
          </View>
        </ScrollView>
      )}
      <View>
        {loginStatus && (
          <BottomRowButton
            title={fundraiser ? 'update progress' : 'Donate'}
            onPress={() => setShareVisibility(ShareVisibility ? false : true)}
            onPress2={() => {
              dispatch({
                type: 'CREATE_DONATION_REQUIRED',
                donation_data: {
                  id: getData._id,
                  Name: getData.user.name,
                  category: getData.category,
                  wallet: getData.wallet,
                },
              });
              navigation.navigate(fundraiser ? 'Update' : 'Donor');
            }}
          />
        )}
      </View>

      <ShareModal
        onPress={() => setShareVisibility(false)}
        visibility={ShareVisibility}
      />
      <DeleteModal
        visibility={DeleteConfirmationModal}
        delete_action={() => {
          dispatch({
            type: 'DELETE_CAMPAIGN',
            payload: {
              id: campaign_data.id,
              setDeleteModal: setDeleteConfirmationModal,
              setModalSuccess: setSuccessModal,
            },
          });
        }}
        cancel_delete={() => setDeleteConfirmationModal(false)}
      />
      <DeleteSuccessModal
        visibility={SuccessModal}
        onPress={() => {
          setSuccessModal(false);
          navigation.navigate('MyCampaign');
        }}
      />
      <FundRaiseModal
        onPress={() => setFundVisibility(false)}
        visibility={FundVisibility}
        onPressUpdate={() => {
          setFundVisibility(false);
          dispatch({
            type: 'CREATE_DONATION_REQUIRED',
            donation_data: {
              token: tokenData,
              id: getData._id,
              Name: getData.user.name,
            },
          });
          navigation.navigate('EditFundraise');
        }}
        deleteAction={() => {
          setFundVisibility(false);
          setDeleteConfirmationModal(true);
        }}
      />
    </View>
  );
}
