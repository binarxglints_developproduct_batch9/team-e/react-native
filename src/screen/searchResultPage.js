import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

// Import Component
import TopBarPage from '../component/topBarPage';
import Card from '../component/myCampaignCard';
import AsyncStorage from '@react-native-async-storage/async-storage';
export default function searchResultPage({navigation}) {
  const dispatch = useDispatch();
  const searchQuery = useSelector((state) => state.searchQuery);
  const searchKeyword = useSelector((state) => state.searchKeyword);
  useEffect(() => {
    console.log(searchQuery);
  }, []);
  const renderItem = ({item}) => {
    console.log('item', item);
    return (
      <Card
        image={item.images}
        title={item.title}
        category={item.category}
        progress={item.total_donation_rupiah / item.goal}
        user={item.user.name}
        raised={item.total_donation_rupiah}
        goal={item.goal}
        onPress={() => {
          const token = AsyncStorage.getItem('token');
          token.then((e) => {
            dispatch({
              type: 'GET_DATA_DONATE_PAGE',
              payload: {
                id: item._id,
                category: item.category,
                navigation,
                token: e,
              },
            });
          });
        }}
      />
    );
  };
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <TopBarPage title="Search Result" nav={navigation} />
      <ScrollView
        style={{
          flex: 1,
        }}>
        <View
          style={{
            backgroundColor: '#fff',
            flex: 1,
            marginTop: 16,
          }}>
          <Text
            style={{
              marginBottom: 16,
              marginHorizontal: 16,
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            Displaying result for '{searchKeyword}'
          </Text>
          <FlatList
            data={searchQuery}
            renderItem={renderItem}
            keyExtractor={searchQuery._id}
          />
        </View>
      </ScrollView>
    </View>
  );
}
