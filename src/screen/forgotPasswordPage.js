import axios from 'axios';
import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LottieView from 'lottie-react-native';

import {API_NO_PARAM_CONFIG} from '../store/reducer/api';
const ScreenWidth = Dimensions.get('window').width;
export default function forgotPasswordPage({navigation}) {
  const [email, setEmail] = useState('');
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [showFailModal, setShowFailModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  function handleForgoTPassword(email) {
    setIsLoading(true);
    axios({
      method: 'POST',
      url: API_NO_PARAM_CONFIG.forgot_password,
      data: {
        email,
      },
    })
      .then((res) => {
        setShowSuccessModal(true);

        setIsLoading(false);
      })
      .catch((err) => {
        setShowFailModal(true);
        setIsLoading(false);
        console.log(err);
      });
  }
  return (
    <ScrollView
      style={{
        flex: 1,
        backgroundColor: '#F1EDE4',
      }}>
      <View style={{flex: 1, backgroundColor: '#F1EDE4', alignItems: 'center'}}>
        <View
          style={{
            padding: 16,
            alignSelf: 'flex-start',
          }}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.goBack()}>
            <Icon name="keyboard-backspace" size={30} color="#1D94A8" />
          </TouchableOpacity>
        </View>
        <View
          style={{
            marginHorizontal: 16,
            flex: 1,
            justifyContent: 'center',
            marginTop: 64,
          }}>
          <Text
            style={{
              fontSize: 42,
              fontWeight: 'bold',
              textTransform: 'capitalize',
              lineHeight: 50,
            }}>
            Forgot your password?
          </Text>
          <Text
            style={{
              letterSpacing: 0.2,
              marginTop: 12,
              fontSize: 16,
              color: '#9f9f9f',
            }}>
            Don't worry , enter your email address and we'll sent the link to
            change it!
          </Text>
          <View>
            <TextInput
              onChangeText={(emails) => {
                setEmail(emails);
              }}
              style={{
                width: ScreenWidth - 32,
                alignItems: 'center',
                backgroundColor: '#FCFCFC',
                borderBottomWidth: 1,
                marginTop: 20,
                paddingHorizontal: 16,
                paddingVertical: 8,
              }}
              keyboardType="email-address"
            />
          </View>
          <TouchableOpacity
            onPress={() => {
              handleForgoTPassword(email);
            }}
            activeOpacity={0.9}
            style={{
              alignItems: 'center',
              width: ScreenWidth - 32,
              borderRadius: 4,
              paddingVertical: 12,
              backgroundColor: '#A43F3C',
              marginTop: 16,
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                color: '#fff',
                textTransform: 'uppercase',
                letterSpacing: 0.9,
              }}>
              Send
            </Text>
          </TouchableOpacity>
        </View>
        <Modal visible={isLoading} transparent={true}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: ScreenWidth - 172,
                alignItems: 'center',
                backgroundColor: '#fff',
                borderRadius: 6,
              }}>
              <View
                style={{width: ScreenWidth - 196, height: ScreenWidth - 196}}>
                <LottieView
                  source={require('../../Asset/JSON Animation/loading.json')}
                  autoPlay
                  loop
                />
              </View>
            </View>
          </View>
        </Modal>
        <Modal visible={showSuccessModal} transparent={true}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'rgba(0, 0, 0, 0.5)',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: ScreenWidth - 64,
                padding: 16,
                borderRadius: 8,
                alignItems: 'center',
                backgroundColor: '#fff',
              }}>
              <View
                style={{
                  width: ScreenWidth - 96,
                  height: 200,
                }}>
                <LottieView
                  source={require('../../Asset/JSON Animation/forgot_password_success.json')}
                  autoPlay
                  loop
                />
              </View>
              <Text
                style={{
                  textTransform: 'uppercase',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  marginTop: 16,
                  color: 'rgba(0, 0, 0, 0.7)',
                }}>
                CHECK your email to reset your password!
              </Text>
              <TouchableOpacity
                onPress={async () => {
                  await setShowSuccessModal(false);
                  navigation.navigate('Login');
                }}
                style={{
                  width: ScreenWidth - 96,
                  marginTop: 12,
                  paddingVertical: 8,
                  borderRadius: 4,
                  backgroundColor: '#A43F3C',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#fff',
                  }}>
                  OK
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </ScrollView>
  );
}
