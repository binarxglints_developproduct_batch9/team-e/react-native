import React from 'react';
import {View, Text, Dimensions, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const screenWidth = Dimensions.get('window').width;

export default function topBarPage({nav, title}) {
  return (
    <View
      style={{
        height: 56,
        width: screenWidth,
        flexDirection: 'row',
        elevation: 2,
        backgroundColor: '#fff',
        alignItems: 'center',

        paddingHorizontal: 16,
      }}>
      <View
        style={{
          position: 'absolute',
          paddingHorizontal: 16,
        }}>
        <TouchableOpacity
          onPress={() => {
            nav.goBack();
          }}>
          <Icon name="keyboard-backspace" size={32} color="#1D94A8" />
        </TouchableOpacity>
      </View>
      <View
        style={{
          width: screenWidth - 32,
          alignItems: 'center',
          flex: 1,
        }}>
        <Text
          style={{
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          {title}
        </Text>
      </View>
    </View>
  );
}
