import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

export function Section1({onPress, title, clicked, color}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#e1e0e0',
        paddingVertical: 16,
        paddingHorizontal: 16,
        flexDirection: 'row',
        backgroundColor: color && 'rgba(215, 235, 238, 0.6)',
      }}>
      <View
        style={{
          flex: 3,
        }}>
        <Text> {title}</Text>
      </View>
      {clicked && (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon name="check" size={24} color="#1D94A8" />
        </View>
      )}
    </TouchableOpacity>
  );
}
export function Section2({onPress, title, clicked, color}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#e1e0e0',
        paddingVertical: 16,
        paddingHorizontal: 16,

        flexDirection: 'row',
        backgroundColor: color && 'rgba(215, 235, 238, 0.6)',
      }}>
      <View
        style={{
          flex: 3,
        }}>
        <Text> {title}</Text>
      </View>
      {clicked && (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon name="check" size={24} color="#1D94A8" />
        </View>
      )}
    </TouchableOpacity>
  );
}
export function Section3({onPress, title, clicked, color}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#e1e0e0',
        paddingVertical: 16,
        paddingHorizontal: 16,

        flexDirection: 'row',
        backgroundColor: color && 'rgba(215, 235, 238, 0.6)',
      }}>
      <View
        style={{
          flex: 3,
        }}>
        <Text> {title}</Text>
      </View>
      {clicked && (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon name="check" size={24} color="#1D94A8" />
        </View>
      )}
    </TouchableOpacity>
  );
}
export function Section4({onPress, title, clicked, color}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        borderBottomWidth: 1,
        borderBottomColor: '#e1e0e0',
        paddingVertical: 16,
        paddingHorizontal: 16,

        flexDirection: 'row',
        backgroundColor: color && 'rgba(215, 235, 238, 0.6)',
      }}>
      <View
        style={{
          flex: 3,
        }}>
        <Text> {title}</Text>
      </View>
      {clicked && (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Icon name="check" size={24} color="#1D94A8" />
        </View>
      )}
    </TouchableOpacity>
  );
}
