import React, {useState} from 'react';
import {View, Text, TextInput, Dimensions} from 'react-native';

const screenWidth = Dimensions.get('window').width;

export default function InputProfile({
  value,
  Title,
  editable,
  span,
  colors,
  fontWeight,
  onChangeText,
  secure,
  placeholder,
}) {
  const [focused, setFocused] = useState(false);

  return (
    <View
      style={{
        alignSelf: 'flex-start',
        marginTop: 16,
      }}>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <Text
          style={{
            color: colors ? '#9f9f9f' : '#000',
            fontSize: 16,
            fontWeight: fontWeight,
          }}>
          {Title}
        </Text>
        <Text
          style={{
            color: '#A43F3C',
            fontSize: 16,
          }}>
          {span}
        </Text>
      </View>

      <View
        style={{
          marginTop: 8,
        }}>
        <TextInput
          placeholder={placeholder}
          onFocus={() => setFocused(true)}
          onBlur={() => setFocused(false)}
          onChangeText={onChangeText}
          value={value}
          editable={editable}
          secureTextEntry={secure}
          style={{
            color: '#000',
            width: screenWidth - 32,
            height: 42,
            backgroundColor: '#fcfcfc',
            fontSize: 14,
            paddingHorizontal: 16,
            fontWeight: 'bold',
            borderBottomWidth: focused ? 1 : 0,
            borderBottomColor: focused ? '#1D94A8' : '#fcfcfc',
          }}
        />
      </View>
    </View>
  );
}
