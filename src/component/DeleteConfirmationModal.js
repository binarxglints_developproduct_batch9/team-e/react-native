import React from 'react';
import {View, Text, TouchableOpacity, Dimensions, Modal} from 'react-native';

const screeenWidth = Dimensions.get('window').width;

export default function DeleteConfirmationModal({
  cancel_delete,
  delete_action,
  visibility,
}) {
  return (
    <Modal visible={visibility} transparent={true}>
      <View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
          backgroundColor: 'rgba(10,10,10,0.56)',
        }}>
        <View
          style={{
            backgroundColor: '#fff',
            padding: 16,
            width: screeenWidth - 96,
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => console.log('ONpRESS')}>
            <Text>Sure Want To Delete This Campaign?</Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',

              marginTop: 8,
            }}>
            <TouchableOpacity
              onPress={delete_action}
              style={{
                flex: 3,
                alignItems: 'flex-end',
              }}>
              <Text
                style={{
                  textTransform: 'uppercase',
                  letterSpacing: 0.5,
                  color: '#A43F3C',

                  fontWeight: 'bold',
                }}>
                YES
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={cancel_delete}
              style={{
                flex: 2,
                marginRight: 16,
                alignItems: 'flex-end',
              }}>
              <Text
                style={{
                  textTransform: 'uppercase',
                  letterSpacing: 0.5,
                  fontWeight: 'bold',
                }}>
                NO
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
}
