import React from 'react';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
const screenWidth = Dimensions.get('window').width;

export default function bottomRowButton({title, nav, onPress, onPress2}) {
  return (
    <View
      style={{
        flexDirection: 'row',
      }}>
      <TouchableOpacity
        activeOpacity={0.7}
        style={{
          backgroundColor: '#fff',
          elevation: 10,
        }}
        onPress={onPress}>
        <View
          style={{
            width: screenWidth - (3 / 5) * screenWidth,
            height: 56,
            backgroundColor: '#fff',

            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <View>
            <Icon name="share" color="#1D94A8" size={24} />
          </View>
          <Text
            style={{
              color: '#1D94A8',
              fontSize: 16,
              textTransform: 'uppercase',
              fontWeight: 'bold',
              letterSpacing: 1,
              marginLeft: 8,
            }}>
            Share
          </Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.8} onPress={onPress2}>
        <View
          style={{
            width: screenWidth - (2 / 5) * screenWidth,
            height: 56,
            backgroundColor: '#A43F3C',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              textTransform: 'uppercase',
              fontWeight: 'bold',
              letterSpacing: 1,
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
