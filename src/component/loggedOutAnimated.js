import React from 'react';
import {View, Text} from 'react-native';
import LottieView from 'lottie-react-native';

export default function loggedOutAnimated({title}) {
  return (
    <View
      style={{
        flex: 1,
      }}>
      <View
        style={{
          flex: 7,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <LottieView
          source={require('../../Asset/JSON Animation/lf30_editor_q8vfhtaj.json')}
          loop
          autoPlay
        />
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontSize: 16,
            color: '#9f9f9f',
          }}>
          {title}
        </Text>
      </View>
    </View>
  );
}
