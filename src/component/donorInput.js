import React, {useState} from 'react';
import {View, Text, TextInput, Dimensions} from 'react-native';

const screenWidth = Dimensions.get('window').width;

export default function donorInput({
  title,
  placeholder,
  keyboard,
  editable,
  value,
  onChangeText,
}) {
  const [Focus, setFocus] = useState(false);
  return (
    <View style={{}}>
      <View
        style={{
          flexDirection: 'row',
          marginBottom: 4,
        }}>
        <Text
          style={{
            fontWeight: 'bold',
          }}>
          {title}
        </Text>
        <Text
          style={{
            color: '#A43F3C',
          }}>
          *
        </Text>
      </View>
      <TextInput
        onChangeText={onChangeText}
        editable={editable}
        value={value}
        placeholder={placeholder}
        keyboardType={keyboard}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        style={{
          width: screenWidth - 32,
          height: 48,
          paddingHorizontal: 16,
          backgroundColor: '#fcfcfc',
          borderBottomColor: !Focus ? '#9f9f9f' : '#1D94A8',
          borderBottomWidth: 2,
        }}
      />
    </View>
  );
}
