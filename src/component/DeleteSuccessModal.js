import React from 'react';
import {View, Text, Modal, Dimensions, TouchableOpacity} from 'react-native';
import LottieView from 'lottie-react-native';
const screenWidth = Dimensions.get('window').width;

export default function DeleteSuccessModal({visibility, onPress}) {
  return (
    <Modal visible={visibility} transparent={true}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'rgba(10,10,10,0.56)',
        }}>
        <View
          style={{
            width: screenWidth - 96,
            padding: 16,
            borderRadius: 8,
            alignItems: 'center',
            backgroundColor: '#fff',
          }}>
          <View>
            <LottieView
              source={require('../../Asset/JSON Animation/40537-garbage.json')}
              autoPlay
              loop
              style={{width: 100, height: 150}}
            />
          </View>
          <Text
            style={{
              fontWeight: 'bold',
              textTransform: 'uppercase',
              letterSpacing: 0.6,
              marginTop: 8,
            }}>
            Campaign Deleted!
          </Text>
          <View
            style={{
              width: screenWidth - 144,
              height: 1,
              backgroundColor: '#9f9f9f',
              marginVertical: 8,
            }}
          />
          <TouchableOpacity onPress={onPress}>
            <Text
              style={{
                fontWeight: 'bold',
                letterSpacing: 0.8,
                color: '#1D94A8',
              }}>
              OK
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}
