import React, {useEffect, useState} from 'react';
import {View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useSelector} from 'react-redux';

const ScreenWidth = Dimensions.get('window').width;

export default function topBar({nav, onPress}) {
  const loginStatus = useSelector((state) => state.loginStatus);
  const [loggedIn, setloggedIn] = useState(false);
  useEffect(() => {
    // status ? setloggedIn(true) : setloggedIn(false);
    // console.log(status);
  }, [loginStatus]);
  const Login = () => {
    return (
      <View>
        <TouchableOpacity
          onPress={() => {
            nav.navigate('Login');
          }}>
          <Text>Login</Text>
        </TouchableOpacity>
      </View>
    );
  };
  const LogOut = () => {
    return <View></View>;
  };
  return (
    <View
      style={{
        elevation: 5,
        height: 56,
        width: ScreenWidth,
        flexDirection: 'row',
        paddingHorizontal: 16,
        paddingVertical: 8,
        backgroundColor: '#fff',
      }}>
      <View
        style={{
          alignSelf: 'flex-start',
          flex: 1,
          paddingVertical: 4,
        }}>
        <TouchableOpacity onPress={onPress}>
          <Image
            source={require('../../Asset/Logo.png')}
            style={{width: 152, height: 32}}
          />
        </TouchableOpacity>
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          flexDirection: 'row',
          justifyContent: 'flex-end',
        }}>
        <View
          style={{
            marginRight: 16,
          }}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              nav.navigate('Search');
            }}>
            <Icon name="search" size={32} color="#1D94A8" />
          </TouchableOpacity>
        </View>
        {!loginStatus && <Login />}
      </View>
    </View>
  );
}
