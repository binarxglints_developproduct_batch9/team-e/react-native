import React from 'react';
import {View, Text, FlatList, Image, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import createdAt from '../component/postCreated';

const screeenWidth = Dimensions.get('window').width;

function Item({image, Name, message, date}) {
  return (
    <View
      style={{
        width: screeenWidth - 72,
        paddingVertical: 12,
        paddingHorizontal: 12,
        borderRadius: 4,
        backgroundColor: '#fff',
        borderTopColor: '#e1e0e0',
        borderTopWidth: 1,

        marginBottom: 8,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1,
        }}>
        {image === 'https://talikasih.kuyrek.com:3000/img/null' ? (
          <Icon name="perm-identity" size={42} />
        ) : (
          <Image
            source={{uri: image}}
            style={{width: 56, height: 56, borderRadius: 4}}
          />
        )}

        <View
          style={{
            flex: 1,
            marginLeft: 8,
            justifyContent: 'center',
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: 'bold',
            }}>
            {Name}
          </Text>
          <View
            style={{
              flex: 1,
            }}>
            <Text
              style={{
                fontSize: 12,
                color: '#9F9F9F',
              }}>
              {createdAt(date)}
            </Text>
          </View>
        </View>
      </View>
      <Text
        style={{
          letterSpacing: 0.3,
          marginTop: 8,
        }}>
        {message}
      </Text>
    </View>
  );
}

export default function problematicFlatlist({data}) {
  const renderItem = ({item}) => {
    return (
      <Item
        image={item.user.profile_image}
        Name={item.user.name}
        message={item.comment}
        date={item.campaign.created_at}
      />
    );
  };
  return (
    <View
      style={{
        alignItems: 'center',
        flex: 1,
      }}>
      <FlatList data={data} keyExtractor={data.id} renderItem={renderItem} />
    </View>
  );
}
