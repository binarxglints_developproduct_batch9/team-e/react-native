import React from 'react';
import {View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import * as Progress from 'react-native-progress';

const screenWidth = Dimensions.get('window').width;

export default function miniCard({
  title,
  image,
  category,
  progress,
  raised,
  goal,
  user,
  onPress,
}) {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: 16,
          borderWidth: 1,
          borderColor: '#F1EDE4',
          borderRadius: 10,
          paddingBottom: 8,
          marginBottom: 32,
          width: screenWidth - 136,
          backgroundColor: '#fff',
          elevation: 4,
        }}>
        <View>
          <Image
            source={{uri: image}}
            style={{
              width: screenWidth - 136,
              height: screenWidth - 230,
              borderTopRightRadius: 10,
              borderTopLeftRadius: 10,
            }}
          />
        </View>
        <View
          style={{
            paddingHorizontal: 12,
            paddingVertical: 4,
            borderColor: '#A43F3C',
            borderWidth: 2,
            borderRadius: 8,
            justifyContent: 'center',
            alignSelf: 'flex-start',
            marginHorizontal: 16,
            marginTop: 16,
            marginBottom: 4,
          }}>
          <Text
            style={{
              color: '#A43F3C',
              fontWeight: 'bold',
              fontSize: 10,
              textTransform: 'capitalize',
            }}>
            {category}
          </Text>
        </View>
        <View
          style={{
            alignSelf: 'flex-start',
            marginHorizontal: 14,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 16,
              textTransform: 'capitalize',
            }}>
            {title}
          </Text>
        </View>
        <View
          style={{
            alignSelf: 'flex-start',
            marginHorizontal: 12,
          }}>
          <Text
            style={{
              fontSize: 12,
            }}>
            {user}
          </Text>
        </View>
        <View
          style={{
            marginTop: 12,
            marginBottom: 8,
          }}>
          <Progress.Bar
            progress={progress}
            width={screenWidth - 168}
            unfilledColor="#f4f4f4"
            color="#1D94A8"
            borderWidth={0}
            borderRadius={30}
            height={10}
          />
        </View>
        <View
          style={{
            width: screenWidth - 168,
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                color: '#9f9f9f',
                fontSize: 14,
              }}>
              Raised
            </Text>
            <Text
              style={{
                color: '#1D94A8',
                fontSize: 12,
                fontWeight: 'bold',
              }}>
              IDR {raised}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <Text
              style={{
                color: '#9f9f9f',
                fontSize: 14,
              }}>
              Goal
            </Text>
            <Text
              style={{
                fontSize: 12,
              }}>
              IDR {goal}
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}
