import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function section({title, value, onPress}) {
  return (
    <View
      style={{
        flexDirection: 'row',
        alignSelf: 'flex-start',
        paddingVertical: 8,
        borderBottomColor: '#e1e0e0',
        borderBottomWidth: 1,
      }}>
      <View
        style={{
          flexDirection: 'row',
          flex: 1,
        }}>
        <Text
          style={{
            fontSize: 16,
          }}>
          {title}
        </Text>
        <Text
          style={{
            fontSize: 16,
            fontWeight: 'bold',
            marginLeft: 4,
          }}>
          ({value})
        </Text>
      </View>
      <View
        style={{
          alignItems: 'flex-end',
          flex: 1,
        }}>
        <TouchableOpacity onPress={onPress}>
          <Icon name="arrow-forward" size={24} color="#000" />
        </TouchableOpacity>
      </View>
    </View>
  );
}
