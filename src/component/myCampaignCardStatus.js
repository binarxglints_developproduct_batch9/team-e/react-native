import React from 'react';
import {View, Text, Image, Dimensions, TouchableOpacity} from 'react-native';
import * as Progress from 'react-native-progress';
import Icon from 'react-native-vector-icons/MaterialIcons';
const screenWidth = Dimensions.get('window').width;

export default function myCampaignCardStatus({
  title,
  image,
  category,
  progress,
  raised,
  goal,
  user,
  onPress,
  status,
  wallet,
}) {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginHorizontal: 24,
          borderWidth: 1,
          borderColor: '#F1EDE4',
          borderRadius: 10,
          paddingBottom: 24,
          marginBottom: 32,
        }}>
        <View>
          <Image
            source={{uri: image}}
            style={{
              width: screenWidth - 48,
              height: screenWidth - 172,
              borderTopRightRadius: 10,
              borderTopLeftRadius: 10,
            }}
          />
        </View>
        <View
          style={{
            alignSelf: 'flex-start',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              paddingHorizontal: 8,
              paddingVertical: 6,
              borderColor: '#A43F3C',
              borderWidth: 2,
              borderRadius: 10,
              justifyContent: 'center',
              alignSelf: 'flex-start',
              marginHorizontal: 16,
              marginTop: 16,
              marginBottom: 8,
            }}>
            <Text
              style={{
                color: '#A43F3C',
                fontWeight: 'bold',
                fontSize: 12,
                textTransform: 'capitalize',
              }}>
              {category}
            </Text>
          </View>
          <View
            style={{
              paddingHorizontal: 8,
              paddingVertical: 6,
              borderColor: '#9f9f9f',
              borderWidth: 2,
              borderRadius: 10,
              justifyContent: 'center',
              alignSelf: 'flex-start',
              marginTop: 16,
              marginBottom: 8,
            }}>
            <Text
              style={{
                color: '#9f9f9f',
                fontWeight: 'bold',
                fontSize: 12,
                textTransform: 'capitalize',
              }}>
              {status}
            </Text>
          </View>
        </View>
        <View
          style={{
            alignSelf: 'flex-start',
            marginHorizontal: 16,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: 18,
              textTransform: 'capitalize',
            }}>
            {title}
          </Text>
        </View>
        <View
          style={{
            alignSelf: 'flex-start',
            marginHorizontal: 16,
          }}>
          <Text
            style={{
              fontSize: 14,
            }}>
            {user}
          </Text>
        </View>
        <View
          style={{
            marginTop: 20,
            marginBottom: 8,
          }}>
          <Progress.Bar
            progress={progress}
            width={screenWidth - 80}
            unfilledColor="#f4f4f4"
            color="#1D94A8"
            borderWidth={0}
            borderRadius={30}
            height={10}
          />
        </View>
        <View
          style={{
            width: screenWidth - 80,
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-start',
            }}>
            <Text
              style={{
                color: '#9f9f9f',
                fontSize: 14,
              }}>
              Raised
            </Text>
            <Text
              style={{
                color: '#1D94A8',
                fontSize: 16,
                fontWeight: 'bold',
              }}>
              IDR {raised}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <Text
              style={{
                color: '#9f9f9f',
                fontSize: 14,
              }}>
              Goal
            </Text>
            <Text
              style={{
                fontSize: 16,
              }}>
              IDR {goal}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            alignItems: 'center',
            alignSelf: 'flex-end',
            marginHorizontal: 16,
            marginTop: 8,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              marginRight: 4,
            }}>
            IDR {wallet}
          </Text>
          <Icon name="account-balance-wallet" size={32} color="#1D94A8" />
        </View>
      </View>
    </TouchableOpacity>
  );
}
