export default function dateParser(date) {
  // const date = '2021-01-30T00:00:00.000Z';
  const dateSplit = date.split('T');
  const getDateSplitter = dateSplit[0].split('-');
  const month = parseInt(getDateSplitter[1]);
  const day = parseInt(getDateSplitter[2]);
  // Current Date
  const currentdate = new Date();
  const currentDay = currentdate.getDay();
  const currentMonth = currentdate.getMonth();
  let daysLeft = 0;
  const monthvalidation = month - currentMonth;
  if (monthvalidation === 0) {
    daysLeft += day - currentDay;
  } else {
    daysLeft += monthvalidation * day - currentDay;
  }
  return daysLeft;
}
