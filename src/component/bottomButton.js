import React from 'react';
import { View, Text, TouchableOpacity, Dimensions } from 'react-native';

const screenWidth = Dimensions.get('window').width;

export default function bottomButton({ title, nav, onPress }) {
  return (
    <View>
      <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
        <View
          style={{
            width: screenWidth,
            height: 56,
            backgroundColor: '#A43F3C',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              textTransform: 'uppercase',
              fontWeight: 'bold',
              letterSpacing: 1,
            }}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
