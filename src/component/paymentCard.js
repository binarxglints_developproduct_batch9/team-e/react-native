import React from 'react';
import {View, Text, TouchableOpacity, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const screenWidth = Dimensions.get('window').width;

export default function paymentCard({focus, onPress, iconName, Title}) {
  return (
    <View>
      <TouchableOpacity onPress={onPress} activeOpacity={0.9}>
        <View
          style={{
            flex: 1,
            paddingHorizontal: 32,
            paddingVertical: 16,
            borderRadius: 6,
            backgroundColor: !focus
              ? 'rgba(215, 235, 238, 0.6)'
              : 'transparent',
            borderColor: '#9f9f9f',
            borderWidth: 1,
            alignItems: 'center',
          }}>
          <Icon name={iconName} size={36} color="#000" />
          <Text
            style={{
              marginTop: 8,
            }}>
            {Title}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
