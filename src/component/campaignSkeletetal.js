import React from 'react';
import {View, Text, Dimensions} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
const screeenWidth = Dimensions.get('window').width;
export default function CampaignSkeletetal() {
  return (
    <SkeletonPlaceholder>
      <View style={{}}>
        <View
          // Image Placeholder
          style={{
            width: screeenWidth,
            height: 242,
          }}
        />
        <View
          // Title Placeholder
          style={{
            marginHorizontal: 16,
            height: 42,
            width: 200,
            borderRadius: 20,
            marginVertical: 24,
          }}
        />
        <View
          style={{
            width: screeenWidth - 16,
            padding: 16,
            borderRadius: 10,
            alignSelf: 'center',
          }}>
          <View
            // Total Donation Placeholder
            style={{
              width: 100,
              height: 32,
              borderRadius: 20,
            }}
          />
          <View
            // Target Placeholder
            style={{
              width: 200,
              height: 24,
              borderRadius: 20,
              marginTop: 8,
            }}
          />
          <View
            // Progress Placeholder
            style={{
              width: screeenWidth - 48,
              height: 24,
              borderRadius: 10,
              marginVertical: 8,
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              // Profile Image Placeholder
              style={{
                width: 56,
                height: 56,
                borderRadius: 6,
                marginRight: 8,
              }}
            />
            <View>
              <View
                style={{
                  width: 54,
                  height: 24,
                  borderRadius: 20,
                }}
              />
              <View
                style={{
                  width: 72,
                  height: 24,
                  borderRadius: 20,
                  marginTop: 4,
                }}
              />
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 8,
            }}>
            <View
              style={{
                width: screeenWidth - 290,

                height: 100,
                borderRadius: 6,
              }}
            />
            <View
              style={{
                width: screeenWidth - 290,

                height: 100,
                borderRadius: 6,
                marginHorizontal: 16,
              }}
            />
            <View
              style={{
                width: screeenWidth - 290,
                height: 100,
                borderRadius: 6,
              }}
            />
          </View>
        </View>
      </View>
    </SkeletonPlaceholder>
  );
}
