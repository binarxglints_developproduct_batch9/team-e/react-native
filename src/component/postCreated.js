export default function timeSplitter(date2) {
  // console.log(date2);
  let finalContainer;
  const container = [];
  // Current Time
  const date = new Date();
  const time = date.toTimeString();
  console.log('current time', time);
  const hours = time.split(' ');
  const splittedHours = hours[0].split(':');

  // Post Created Time
  const stringify = new Date(date2);
  const time2 = stringify.toTimeString();

  const hours2 = time2.split(' ');
  const splittedHours2 = hours2[0].split(':');

  // Operations
  for (let i = 0; i < splittedHours.length; i++) {
    container.push(parseInt(splittedHours[i]) - parseInt(splittedHours2[i]));
  }
  if (container[0] > 0) {
    finalContainer = container[0] + ' hours ago';
  } else if (container[0] == 0) {
    finalContainer = container[1] + ' ' + 'minutes ago';
  } else if (container[0] < 0) {
    finalContainer = parseInt(container[1]) + ' ' + 'hours ago';
  }
  return finalContainer;
}
