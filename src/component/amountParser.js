export default function amountParser(arr) {
  const stringify = JSON.stringify(arr);
  const reversed = [];
  const container = [];
  for (let i = stringify.length - 1; i >= 0; i--) {
    reversed.push(stringify[i]);
  }
  for (let i = 0; i < reversed.length; i += 4) {
    reversed.splice(i, 0, '.');
  }
  for (let i = 1; i < reversed.length; i++) {
    container.push(reversed[i]);
  }
  // const substring = reversed.shift();
  const final = container.reverse().join('');
  return final;
}
