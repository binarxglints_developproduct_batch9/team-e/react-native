import React from 'react';
import {View, Text, Dimensions, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Modal from 'react-native-modal';

const screeenWidth = Dimensions.get('window').width;

export function ShareModal({onPress, visibility}) {
  return (
    <Modal
      animationIn="slideInUp"
      hideModalContentWhileAnimating={true}
      isVisible={visibility}
      style={{justifyContent: 'flex-end', margin: 0}}>
      <View
        style={{
          width: screeenWidth,
          height: 224,
          backgroundColor: '#fff',
          paddingVertical: 16,
          paddingHorizontal: 28,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              flex: 1,
            }}>
            Help by sharing
          </Text>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity onPress={onPress}>
              <Icon name="x" size={24} color="#2d2d2d" />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            width: screeenWidth - 56,
            backgroundColor: '#fcfcfc',
            borderBottomWidth: 2,
            borderBottomColor: '#9d9d9d',
            height: 48,
            alignItems: 'center',
            paddingHorizontal: 16,
            marginTop: 16,
            marginBottom: 16,
          }}>
          <View
            style={{
              marginRight: 8,
            }}>
            <Icon name="link" color="#1D94A8" size={16} />
          </View>
          <View
            style={{
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 14,
                color: '#2D2D2D',
                alignSelf: 'center',
                justifyContent: 'center',
              }}>
              https://talikasih.com/aid-for-necessary-it...
            </Text>
          </View>
        </View>
        <View
          style={{
            width: screeenWidth - 56,
            paddingVertical: 16,
            alignItems: 'center',
            backgroundColor: '#A43F3C',
            borderRadius: 4,
          }}>
          <Text
            style={{
              fontSize: 16,
              fontWeight: 'bold',
              color: '#fff',
              textTransform: 'uppercase',
            }}>
            Copy link
          </Text>
        </View>
      </View>
    </Modal>
  );
}

export function FundRaiseModal({
  onPress,
  visibility,
  onPressUpdate,
  deleteAction,
}) {
  return (
    <Modal
      animationIn="slideInUp"
      hideModalContentWhileAnimating={true}
      isVisible={visibility}
      style={{justifyContent: 'flex-end', margin: 0}}>
      <View
        style={{
          width: screeenWidth,
          height: 220,
          backgroundColor: '#fff',
          paddingHorizontal: 24,
          paddingVertical: 24,
        }}>
        <View
          style={{
            flexDirection: 'row',
            marginBottom: 8,
          }}>
          <Text style={{fontSize: 18, fontWeight: 'bold'}}>
            Manage Campaign
          </Text>
          <View
            style={{
              flex: 1,
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity onPress={onPress}>
              <Icon name="x" size={24} color="2d2d2d" />
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          onPress={onPressUpdate}
          activeOpacity={0.7}
          style={{paddingVertical: 16, paddingHorizontal: 4}}>
          <Text style={{fontSize: 16}}>Edit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          style={{paddingVertical: 16, paddingHorizontal: 4}}>
          <Text style={{fontSize: 16}}>Close Campaign</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={deleteAction}
          activeOpacity={0.7}
          style={{paddingVertical: 16, paddingHorizontal: 4}}>
          <Text style={{fontSize: 16, color: '#A43F3C'}}>Delete</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
}
