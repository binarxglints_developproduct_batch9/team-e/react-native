import React from 'react';
import {View, Text, FlatList, Image, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import amountParser from '../component/amountParser';
const screeenWidth = Dimensions.get('window').width;
import postCreated from '../component/postCreated';

function Item({image, Name, message, amount, date}) {
  return (
    <View
      style={{
        width: screeenWidth - 72,
        paddingVertical: 12,
        paddingHorizontal: 12,
        borderRadius: 4,
        backgroundColor: '#fff',
        borderColor: '#979797',
        borderWidth: 1,
        borderColor: '#E1E0E0',
        marginBottom: 16,
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        {image === 'https://talikasih.kuyrek.com:3000/img/null' ? (
          <Icon name="perm-identity" size={42} />
        ) : (
          <Image
            source={{uri: image}}
            style={{width: 50, height: 50, borderRadius: 4}}
          />
        )}

        <View
          style={{
            flex: 1,
            marginLeft: 4,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View
              style={{
                flex: 1,
              }}>
              <Text
                style={{
                  justifyContent: 'flex-start',
                  color: '#1D94A8',
                  fontWeight: 'bold',
                }}>
                IDR {amount}
              </Text>
            </View>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'flex-end',
                flex: 1,
              }}>
              <Text
                style={{
                  fontSize: 12,
                  color: '#9F9F9F',
                }}>
                {postCreated(date)}
              </Text>
            </View>
          </View>
          <Text>{Name}</Text>
        </View>
      </View>
      <Text
        style={{
          letterSpacing: 0.3,
          marginTop: 4,
        }}>
        {message}
      </Text>
    </View>
  );
}

export default function problematicFlatlist({Comment}) {
  const renderItem = ({item}) => {
    // console.log(item.campaign.created_at);
    return (
      <Item
        image={item.user.profile_image}
        amount={item.amount ? amountParser(item.amount) : '000'}
        Name={item.user.name}
        message={item.message}
        date={item.campaign.created_at}
      />
    );
  };
  return (
    <View
      style={{
        alignItems: 'center',
        flex: 1,
      }}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={Comment}
        keyExtractor={Comment.id}
        renderItem={renderItem}
      />
    </View>
  );
}
