import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import RootSaga from '../sagas/rootSaga';

// Import Reducer
import Reducer from './reducer/reducer';

const saga = createSagaMiddleware();
export const Store = createStore(Reducer, applyMiddleware(saga));
saga.run(RootSaga);
