import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {API_NO_PARAM_CONFIG} from '../reducer/api';

export default function CREATE_CAMPAIGN(payload) {
  const data = new FormData();

  data.append('title', payload.title);
  data.append('category', payload.category);
  data.append('due_date', payload.date);
  data.append('goal', payload.goal);
  data.append('story', payload.story);
  data.append('images', {
    name: payload.image.fileName,
    type: payload.image.type,
    uri: payload.image.uri,
  });
  const token = AsyncStorage.getItem('token');
  token.then((e) => {
    axios({
      method: 'POST',
      url: API_NO_PARAM_CONFIG.createCampaign,
      headers: {
        Authorization: `Bearer ${e}`,
        'Content-Type': 'multipart/form-data' + 'boundary=' + data._boundary,
        accept: 'application/json',
      },
      data,
    })
      .then((res) => {
        payload.loading(false);
        payload.setIsValid(true);
      })
      .catch((err) => console.log(err, data));
  });
}
