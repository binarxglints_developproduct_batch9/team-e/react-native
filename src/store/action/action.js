import AsyncStorage from '@react-native-async-storage/async-storage';

export const deleteToken = () => {
  try {
    AsyncStorage.removeItem('token');
    return true;
  } catch (e) {
    return false;
  }
};


