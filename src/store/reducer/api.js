export const API_NO_PARAM_CONFIG = {
  user: 'https://talikasih.kuyrek.com:3000/user',
  userUpdate: 'https://talikasih.kuyrek.com:3000/user/update',
  newest: 'https://talikasih.kuyrek.com:3001/campaign/new?page=1&limit=10',
  urgent: 'https://talikasih.kuyrek.com:3001/campaign/urgen?page=1&limit=1',
  popular: 'https://talikasih.kuyrek.com:3001/campaign/populer?page=1&limit=10',
  createCampaign: 'https://talikasih.kuyrek.com:3001/campaign/create',
  createComment: 'https://talikasih.kuyrek.com:3004/comment/create',
  midtrans: 'https://talikasih.kuyrek.com:3002/donation/create/midtrans/',
  donation_bank_transfer: 'https://talikasih.kuyrek.com:3002/donation/create',
  campaignProgress: 'https://talikasih.kuyrek.com:3003/update/create',
  campaignWithdrawal:
    'https://talikasih.kuyrek.com:3003/update/create/withdraw',
  forgot_password: 'https://talikasih.kuyrek.com:3000/user/forgotpassword',
};
