const initialState = {
  token: '',
  Name: '',
  email: '',
  password: '',
  bankName: '',
  bankNumber: '',
  donationTitle: '', // To Pass value from page to page
  loginStatus: false,
  userID: '',
  campaignID: '',
  userData: [],
  filter: false,
  response: [],
  searchQuery: [],
  category: '',
  fundraiser: false,
  donation_data: [],
  response: false,
  campaign_data: [],
  campaign_data_response: [],
  comment_data_response: [],
  related_campaign_response: [],
  campaign_update_response: [],
  donation_data_response: [],
  urgent_data_response: [],
  newest_data_response: [],
  popular_data_reponse: [],
  searchKeyword: '',
  midtransData: [],
  userLoginData: [],
};

export default function Reducer(state = initialState, action) {
  const {
    type,
    campID,
    payloadEmail,
    payloadPassword,
    payloadName,
    payloadToken,
    payloadTitle,
    payloadBankName,
    payloadBankNumber,
    payloadUserID,
    status,
    data,
    payloadFilter,
    payloadResponse,
    payloadSearch,
    payloadCategory,
    fundraise,
    donation_data,
    response,
    campaign_data,
    campaign_data_responses,
    comment_data_response,
    related_campaign_response,
    campaign_update_response,
    donation_data_response,
    urgent_data_response,
    newest_data_response,
    popular_data_reponse,
    searchKeyword,
    midtransData,
    loginData,
  } = action;
  switch (type) {
    case 'Login':
      return {
        ...state,
        payload,
      };
    case 'campaign':
      return {
        ...state,
        campaignID: campID,
      };
    case 'status':
      return {
        ...state,
        loginStatus: status,
      };
    case 'userID':
      return {
        ...state,
        userID: payloadUserID,
      };
    case 'UserData':
      return {
        ...state,
        userData: data,
      };
    case 'Filter':
      return {
        ...state,
        filter: payloadFilter,
      };
    case 'CREATE_CAMPAIGN_RESPONSE':
      return {
        ...state,
        response: payloadResponse,
      };
    case 'SearchQuery':
      return {
        ...state,
        searchQuery: payloadSearch,
      };
    case 'category':
      return {
        ...state,
        category: payloadCategory,
      };
    case 'fundraise':
      return {
        ...state,
        fundraiser: fundraise,
      };
    case 'CREATE_DONATION_REQUIRED':
      return {
        ...state,
        donation_data: donation_data,
      };
    case 'CREATE_DONATION_RESPONSE':
      return {
        ...state,
        response,
      };
    case 'CAMPAIGN_DATA_REQUIRED':
      return {
        ...state,
        campaign_data,
      };
    case 'CAMPAIGN_DATA_RESPONSE':
      return {
        ...state,
        campaign_data_response: campaign_data_responses,
      };
    case 'COMMENT_RESPONSE':
      return {
        ...state,
        comment_data_response: comment_data_response,
      };
    case 'RELATED_CAMPAIGN_RESPONSE':
      return {
        ...state,
        related_campaign_response: related_campaign_response,
      };
    case 'UPDATE_CAMPAIGN_RESPONSE':
      return {
        ...state,
        campaign_update_response,
      };
    case 'DONATION_RESPONSE':
      return {
        ...state,
        donation_data_response,
      };
    case 'URGENT_DATA':
      return {
        ...state,
        urgent_data_response,
      };
    case 'NEWEST_DATA':
      return {
        ...state,
        newest_data_response,
      };
    case 'POPULAR_DATA':
      return {
        ...state,
        popular_data_reponse,
      };
    case 'searchKeyword':
      return {
        ...state,
        searchKeyword,
      };
    case 'MIDTRANS_INFO_REQUIRED':
      return {
        ...state,
        midtransData,
      };
    case 'LOGIN_DATA':
      return {
        ...state,
        userLoginData: loginData,
      };
    default:
      return {
        ...state,
      };
  }
}
