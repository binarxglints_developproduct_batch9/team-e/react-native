import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {call, fork, put, takeLatest} from 'redux-saga/effects';
import {API_NO_PARAM_CONFIG} from '../store/reducer/api';
import CREATE_CAMPAIGN from '../store/action/CREATE_CAMPAIGN';

function* PostCreateCampaign(action) {
  const response = yield call(CREATE_CAMPAIGN, action.payload);
  yield put({type: 'CREATE_CAMPAIGN_RESPONSE', payloadResponse: response});
}

function UPDATE_EMAIL(data) {
  axios({
    method: 'PUT',
    url: `${API_NO_PARAM_CONFIG.userUpdate}/email`,
    headers: {
      Authorization: 'Bearer' + ' ' + data.token,
    },
    data: {
      email: data.email,
    },
  })
    .then((e) => {
      alert('EMAIL UPDATED');
    })
    .catch((e) => {
      alert(e);
      console.log(data.token);
    });
}

function* UpdateEmail(action) {
  const response = yield call(UPDATE_EMAIL, action.payload);
}

function UPDATE_BANK_INFO(data) {
  axios({
    method: 'PUT',
    url: `${API_NO_PARAM_CONFIG.userUpdate}/profile`,
    headers: {
      Authorization: `Bearer ${data.token}`,
    },
    data: {
      name: data.name,
      bank_name: data.bank_name,
      bank_account_number: data.bank_number,
    },
  })
    .then((e) => {
      alert('PROFILE UPDATED SUCCESFULLY');
    })
    .catch((e) => {
      alert(e);
      console.log('profile', e);
    });
}
function* UpdateBankInfo(action) {
  action.payload.name !== ''
    ? yield call(UPDATE_BANK_INFO, action.payload)
    : console.log('name is empty');
}

function UPDATE_PROFILE_IMAGE(datas) {
  const data = new FormData();
  data.append('profile_image', {
    name: datas.image.fileName,
    type: datas.image.type,
    uri: datas.image.uri,
  });
  axios({
    method: 'PUT',
    url: `${API_NO_PARAM_CONFIG.userUpdate}/image`,
    headers: {
      Authorization: `Bearer ${datas.token}`,
      'Content-Type': 'multipart/form-data' + 'boundary=' + data._boundary,
      accept: 'application/json',
    },
    data: data,
  })
    .then((e) => {
      alert('PROFILE IMAGE UPDATED');
    })
    .catch((e) => {
      alert(e);
      console.log('image', e);
    });
}
function* UpdateProfileImage(action) {
  const response = yield call(UPDATE_PROFILE_IMAGE, action.payload);
}

function UPDATE_PASSWORD(data) {
  data.password !== '' &&
    axios({
      method: 'PUT',
      url: `${API_NO_PARAM_CONFIG.userUpdate}/password`,
      headers: {
        Authorization: `Bearer ${data.token}`,
      },
      data: {
        password: data.password,
        passwordConfirmation: data.repPass,
      },
    })
      .then((e) => alert('PASSWORD UPDATED'))
      .catch((e) => {
        // alert(e);
        console.log('password', e);
      });
}
function modalIsVisible(data) {
  data.visible(true);
}

function* UpdatePassword(action) {
  const response = yield call(UPDATE_PASSWORD, action.payload);
  yield call(modalIsVisible, action.payload);
}
function deleteAction(data) {
  const token = AsyncStorage.getItem('token');
  token.then((tooken) => {
    axios({
      method: 'DELETE',
      url: `https://talikasih.kuyrek.com:3001/campaign/delete/${data.id}`,
      headers: {
        Authorization: `Bearer ${tooken}`,
      },
    })
      .then((res) => {
        data.setDeleteModal(false);
        data.setModalSuccess(true);
      })
      .catch((e) => {
        alert('UNABLE TO DELETE CAMPAIGN');
      });
  });
}

function* deleteCampaign(action) {
  yield call(deleteAction, action.payload);
}

function create_comment_action(data) {
  axios({
    method: 'POST',
    url: API_NO_PARAM_CONFIG.createComment,
    headers: {
      Authorization: `Bearer ${data.token}`,
    },
    data: {
      campaign_id: data.id,
      comment: data.comment,
    },
  })
    .then((e) => alert('Comment Created'))
    .catch((e) => alert(e));
}

function* CREATE_COMMENT_REQUESTED(action) {
  yield call(create_comment_action, action.payload);
}

function create_donation_action(data) {
  const response = axios({
    method: 'POST',
    url: API_NO_PARAM_CONFIG.midtrans,
    headers: {
      Authorization: `Bearer ${data.token}`,
    },
    data: {
      campaign: data.id,
      amount: data.amount,
      message: data.message,
      name: data.name,
    },
  })
    .then((e) => {
      data.navigation.navigate('midtrans');
      return e.data.data;
    })
    .catch((e) => {
      console.log(e, data);
      return false;
    });
  return response;
}

function* createDonation(action) {
  try {
    const response = yield call(create_donation_action, action.payload);
    yield put({type: 'MIDTRANS_INFO_REQUIRED', midtransData: response});
  } catch (error) {
    yield put({type: 'CREATE_DONATION_RESPONSE', payloadResponse: response});
  }
}

function edit_campaign_action(data) {
  axios({
    method: 'POST',
    url: `https://talikasih.kuyrek.com:3001/campaign/update/campaign/${data.id}`,
    headers: {
      Authorization: `Bearer ${data.token}`,
    },
    data: {
      title: data.title,
      goal: data.goal,
      due_date: data.due_date,
      category: data.category,
      story: data.story,
    },
  })
    .then((e) => {
      alert('EDIT CAMPAIGN SUCCEED');
    })
    .catch((e) => alert(e));
}

function* editCampaign(action) {
  const response = yield call(edit_campaign_action, action.payload);
}

function update_progress_action(datas) {
  const token = AsyncStorage.getItem('token');
  token
    .then((e) => {
      axios({
        method: 'POST',
        url: API_NO_PARAM_CONFIG.campaignProgress,
        headers: {
          Authorization: `Bearer ${e}`,
        },
        data: {
          message: datas.message,
          campaign_id: datas.campaign_id,
        },
      })
        .then((e) => {
          datas.loading(false);
          alert('update succeed');
        })
        .catch((e) => {
          console.log(e);
          datas.loading(false);
          alert(e);
        });
    })
    .catch((e) => alert('Please Log In'));
}

function* updateProgress(action) {
  const response = yield call(update_progress_action, action.payload);
}

function withdrawal_update_action(data) {
  axios({
    method: 'POST',
    url: API_NO_PARAM_CONFIG.campaignWithdrawal,
    headers: {
      Authorization: `Bearer ${data.token}`,
    },
    data: {
      amount: data.amount,
      message: data.message,
      campaign_id: data.id,
    },
  })
    .then((e) => {
      data.loading(false);
      data.succeed(true);
      data.succeed(true);
    })
    .catch((e) => {
      data.loading(false);

      alert(e);
    });
}

function* withdrawalUpdate(action) {
  const response = yield call(withdrawal_update_action, action.payload);
}

function get_campaign_data(payload) {
  const response =
    payload.token !== null
      ? axios({
          // GET CAMPAIGN DATA LOGGED IN
          method: 'GET',
          url: `https://talikasih.kuyrek.com:3001/campaign/getone/${payload.id}`,
          headers: {
            Authorization: `Bearer ${payload.token}`,
          },
        })
          .then((res) => {
            return res.data.data;
          })
          .catch((err) => {
            return err;
          })
      : axios({
          // GET CAMPAIGN DATA LOGGED OUT
          method: 'GET',
          url: `https://talikasih.kuyrek.com:3001/campaign/getone/${payload.id}`,
        })
          .then((res) => {
            return res.data.data;
          })
          .catch((err) => err);
  return response;
}

function get_comment(data) {
  const response = axios({
    method: 'GET',
    url: `https://talikasih.kuyrek.com:3004/comment/get/campaign?campaign_id=${data.id}&page=1&limit=10`,
  })
    .then((res) => {
      return res.data.data;
    })
    .catch((err) => err);
  return response;
}

function get_related_campaign(data) {
  const response = axios({
    // GET RELATED CAMPAIGN
    method: 'GET',
    url: `https://talikasih.kuyrek.com:3001/campaign/category?category=${data.category}&page=1&limit=10`,
  })
    .then((e) => {
      return e.data.data;
    })
    .catch((err) => err);
  return response;
}

function get_update_campaign(data) {
  const response = axios({
    // GET CAMPAIGN UPDATE (TIMELINE)
    method: 'GET',
    url: `https://talikasih.kuyrek.com:3003/update/get/?campaign_id=${data.id}`,
  })
    .then((e) => {
      return e.data.data;
    })
    .catch((e) => e);
  return response;
}

function get_donation(data) {
  const response = axios({
    method: 'GET',
    url: `https://talikasih.kuyrek.com:3002/donation/campaign/?campaign_id=${data.id}&page=1&limit=10`,
  })
    .then((e) => {
      return e.data.data;
    })
    .catch((e) => e);
  return response;
}

function navigate(data) {
  data.navigation.navigate('DonatePage');
  setTimeout(() => {
    data.setIsLoading(false);
  }, 100);
}

function* getDataDonatePage(action) {
  const campaign_data_reponse = yield call(get_campaign_data, action.payload);
  yield put({
    type: 'CAMPAIGN_DATA_RESPONSE',
    campaign_data_responses: campaign_data_reponse,
  });
  const comment_response = yield call(get_comment, action.payload);
  yield put({
    type: 'COMMENT_RESPONSE',
    comment_data_response: comment_response,
  });
  const relate_campaign_reponse = yield call(
    get_related_campaign,
    action.payload,
  );
  yield put({
    type: 'RELATED_CAMPAIGN_RESPONSE',
    related_campaign_response: relate_campaign_reponse,
  });
  const update_campaign_reponse = yield call(
    get_update_campaign,
    action.payload,
  );
  yield put({
    type: 'UPDATE_CAMPAIGN_RESPONSE',
    campaign_update_response: update_campaign_reponse,
  });
  const donation_response = yield call(get_donation, action.payload);
  yield put({
    type: 'DONATION_RESPONSE',
    donation_data_response: donation_response,
  });

  yield call(navigate, action.payload);
}

function get_urgent(data) {
  axios({
    method: 'GET',
    url: `https://talikasih.kuyrek.com:3001/campaign/urgen?page=1&limit=10`,
  })
    .then((e) => e.data.data)
    .catch((e) => e);
}

function* getDataHomePage(action) {
  const urgentResponse = yield call(get_urgent, action.payload);
  yield put('URGENT_DATA', urgentResponse);
  const newestResponse = yield call(get_newest);
  yield put(`NEWEST_DATA`, newestResponse);
  const popularResponse = yield call(get_popular);
  yield put('POPULAR_DATA', popularResponse);
}
function search_get_data(data) {
  const response = axios({
    method: 'GET',
    url: `https://talikasih.kuyrek.com:3001/campaign/title?title=${data.searchQuery}&page=1&limit=10`,
  })
    .then((e) => {
      return e.data.data;
    })
    .catch((e) => e);
  return response;
}

function logger(response) {
  console.log(response);
}
function navigates(data) {
  data.navigation.navigate('searchResult');
}

function* search_workers(action) {
  const response = yield call(search_get_data, action.payload);
  yield put({type: 'SearchQuery', payloadSearch: response});
  yield put({type: 'searchKeyword', searchKeyword: action.payload.searchQuery});

  yield call(navigates, action.payload);
}
function CREATE_BANK_TRANSFER_WORKER(datas) {
  const data = new FormData();
  data.append('verification_images', {
    name: datas.image.fileName,
    type: datas.image.type,
    uri: datas.image.uri,
  });
  data.append('amount', datas.amount);
  data.append('message', datas.message);
  data.append('name', datas.name);
  data.append('campaign', datas.id);

  axios({
    method: 'POST',
    url: API_NO_PARAM_CONFIG.donation_bank_transfer,
    headers: {
      Authorization: 'Bearer ' + datas.token,
    },
    data,
  })
    .then((e) => {
      datas.visible(true);
    })
    .catch((e) => {
      console.log('error', e);
    });
}

function* CREATE_BANK_TRANSFER_REQUESTED(action) {
  const response = yield call(CREATE_BANK_TRANSFER_WORKER, action.payload);
}
// Saga Testing

export default function* RootSaga() {
  yield takeLatest('CREATE_CAMPAIGN_REQUSTED', PostCreateCampaign);
  yield takeLatest('UPDATE_EMAIL_REQUESTED', UpdateEmail);
  yield takeLatest('UPDATE_BANKINFO_REQUESTED', UpdateBankInfo);
  yield takeLatest('UPDATE_PROFILE_IMAGE_REQUESTED', UpdateProfileImage);
  yield takeLatest('UPDATE_PASSWORD_REQUESTED', UpdatePassword);
  yield takeLatest('DELETE_CAMPAIGN', deleteCampaign);
  yield takeLatest('CREATE_COMMENT_REQUESTED', CREATE_COMMENT_REQUESTED);
  yield takeLatest('CREATE_DONATION_REQUESTED', createDonation);
  yield takeLatest(
    'CREATE_TRANSFER_DONATION_REQUESTED',
    CREATE_BANK_TRANSFER_REQUESTED,
  );

  yield takeLatest('EDIT_CAMPAIGN_REQUESTED', editCampaign);
  yield takeLatest('UPDATE_PROGRESS_REQUESTED', updateProgress);
  yield takeLatest('WITHDRAWAL_UPDATE_REQUESTED', withdrawalUpdate);
  yield takeLatest('GET_DATA_DONATE_PAGE', getDataDonatePage);
  yield takeLatest('GET_DATA_HOME_PAGE', getDataHomePage);
  yield takeLatest('SEARCH_REQUESTED', search_workers);
}
