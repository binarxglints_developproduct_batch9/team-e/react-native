import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Image, View} from 'react-native';
// Import Screen
import Search from './src/screen/searchPage';
import HomePage from './src/screen/homePage';
import CreateCampaign from './src/screen/createCampaign';
import MyAccount from './src/screen/myAccount';
import MorePage from './src/screen/morePage';
import SplashScreen from './src/screen/splashScreen';
import Login from './src/screen/loginPage';
import Register from './src/screen/registerPage';
import EditProfile from './src/screen/editProfile';
import MyDonation from './src/screen/myDonation';
import MyCampaign from './src/screen/myCampaign';
import Donate from './src/screen/donatePage';
import Donor from './src/screen/donorPage';
import Update from './src/screen/updateFundraise';
import Filter from './src/screen/filterPage';
import EditFundraise from './src/screen/editCampaign';
import SearchResult from './src/screen/searchResultPage';
import Forgot_password_page from './src/screen/forgotPasswordPage';
import MidtransPage from './src/screen/midtransWebview';
// Import Store
import {Provider} from 'react-redux';
import {Store} from './src/store/store';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const App: () => React$Node = () => {
  function BottomTabs() {
    return (
      <Tab.Navigator
        detachInactiveScreens={true}
        tabBarOptions={{
          activeTintColor: '#1D94A8',
        }}>
        <Tab.Screen
          name="Donate"
          component={HomePage}
          options={{
            tabBarIcon: (props) => {
              return (
                <View>
                  <Icon
                    name="grade"
                    size={32}
                    color={props.focused ? '#1D94A8' : 'grey'}
                  />
                </View>
              );
            },
          }}
        />
        <Tab.Screen
          name="Create Campaign"
          component={CreateCampaign}
          options={{
            tabBarIcon: (props) => {
              return (
                <View>
                  <Icon
                    name="add-box"
                    size={32}
                    color={props.focused ? '#1D94A8' : 'grey'}
                  />
                </View>
              );
            },
          }}
        />
        <Tab.Screen
          name="My Account"
          component={MyAccount}
          options={{
            tabBarIcon: (props) => {
              return (
                <View>
                  <Icon
                    name="person-outline"
                    size={32}
                    color={props.focused ? '#1D94A8' : 'grey'}
                  />
                </View>
              );
            },
          }}
        />
        <Tab.Screen
          name="More"
          component={MorePage}
          options={{
            tabBarIcon: (props) => {
              return (
                <View>
                  <Icon
                    name="more-horiz"
                    size={32}
                    color={props.focused ? '#1D94A8' : 'grey'}
                  />
                </View>
              );
            },
          }}
        />
      </Tab.Navigator>
    );
  }

  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashScreen">
          <Stack.Screen
            name="SplashScreen"
            component={SplashScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Register"
            component={Register}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Search"
            component={Search}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Filter"
            component={Filter}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="EditProfile"
            component={EditProfile}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="BottomTabbed"
            component={BottomTabs}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="MyDonation"
            component={MyDonation}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="MyCampaign"
            component={MyCampaign}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="DonatePage"
            component={Donate}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Donor"
            component={Donor}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Update"
            component={Update}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="EditFundraise"
            component={EditFundraise}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="searchResult"
            component={SearchResult}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="forgot_password"
            component={Forgot_password_page}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="midtrans"
            component={MidtransPage}
            options={{
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
